import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger

pd.set_option('display.width', 1000)

DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/april_2018/big_grid_vgg_cifar10_3_relu"
FILENAME = "gathered_results.csv"

min_acc = 0.825
max_acc = 0.95
linewidth = 0.9
capsize = 3
output_conv_dim = 512
kernel_marker = {
    "chi2_cpd_lin": "x",
    "rbf_lin": "d",
    "chi2_cpd_nonlin": "h",
    "rbf_nonlin": "v",
}
kernel_color = {
    "chi2_cpd_lin": "b",
    "rbf_lin": "c",
    "chi2_cpd_nonlin": "r",
    "rbf_nonlin": "m",
}

dense_marker = "+"

dense_color = "k"

deepfried_marker = "8"

deepfried_color = "#F289E7"

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    df = df.apply(pd.to_numeric, errors="ignore")
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    repr_dim = set(df["repr_dim"].values)
    repr_dim.remove("None")  # dtype: str
    nys_size = 128
    seed_values = set(df["seed"].values)
    batch_size = 128
    logger.debug("Nystrom size is {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))


    means_deepstrom = {}

    # plot deepstrom
    # ==============
    df_deepstrom = df[df["method_name"] == "deepstrom"]
    df_deepstrom["repr_dim"] = df_deepstrom["repr_dim"].astype(np.int)
    df_deepstrom = df_deepstrom.sort_values(by=["repr_dim"])
    for k_name in kernel_names:
        df_deepstrom_kernel = df_deepstrom[df_deepstrom["kernel_deepstrom"] == k_name]
        # get the results of learned nystrom
        df_deepstrom_kernel_w = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"] == False]
        # get the results without the non lienar activation function
        df_deepstrom_kernel_w_lin = df_deepstrom_kernel_w[df_deepstrom_kernel_w["non_linearity"] == str(None)]
        np_deepstrom_kernel_w_lin_mean_accuracy_test = np.mean(
            np.array([list(df_deepstrom_kernel_w_lin[df_deepstrom_kernel_w_lin["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        np_deepstrom_kernel_w_lin_std_accuracy_test = np.std(
            np.array([list(df_deepstrom_kernel_w_lin[df_deepstrom_kernel_w_lin["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        plt.errorbar(sorted([int(n) for n in repr_dim]),
                    np_deepstrom_kernel_w_lin_mean_accuracy_test,
                    np_deepstrom_kernel_w_lin_std_accuracy_test,
                    color=kernel_color[k_name+"_lin"],
                    marker=kernel_marker[k_name+"_lin"],
                    label="Deepstrom linear; kernel: {}".format(k_name),
                    linestyle='None',
                    capsize=capsize)
        # get the results with the non linear activation function
        df_deepstrom_kernel_w_nonlin = df_deepstrom_kernel_w[df_deepstrom_kernel_w["non_linearity"] != str(None)]
        np_deepstrom_kernel_w_nonlin_mean_accuracy_test = np.mean(
            np.array([list(df_deepstrom_kernel_w_nonlin[df_deepstrom_kernel_w_nonlin["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        np_deepstrom_kernel_w_nonlin_std_accuracy_test = np.std(
            np.array([list(df_deepstrom_kernel_w_nonlin[df_deepstrom_kernel_w_nonlin["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        plt.errorbar(sorted([int(n) for n in repr_dim]),
                    np_deepstrom_kernel_w_nonlin_mean_accuracy_test,
                    np_deepstrom_kernel_w_nonlin_std_accuracy_test,
                    color=kernel_color[k_name+"_nonlin"],
                    marker=kernel_marker[k_name+"_nonlin"],
                    linestyle='None',
                    label="Deepstrom non linear; kernel: {}".format(k_name),
                    capsize=capsize)
        # get the results of vanilla nystrom
        df_deepstrom_kernel_k = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"]]
        if len(df_deepstrom_kernel_k):
            rep_dim = df_deepstrom_kernel_k["repr_dim"].values[0]
            # get the results without the non lienar activation function
            df_deepstrom_kernel_k_lin = df_deepstrom_kernel_k[df_deepstrom_kernel_k["non_linearity"] == str(None)]
            np_deepstrom_kernel_k_lin_mean_accuracy_test = np.mean(
                np.array(
                    [list(df_deepstrom_kernel_k_lin[df_deepstrom_kernel_k_lin["seed"] == seed_v]["accuracy_test"]) for
                     seed_v in
                     seed_values]), axis=0)
            np_deepstrom_kernel_k_lin_std_accuracy_test = np.std(
                np.array(
                    [list(df_deepstrom_kernel_k_lin[df_deepstrom_kernel_k_lin["seed"] == seed_v]["accuracy_test"]) for
                     seed_v in
                     seed_values]), axis=0)
            plt.errorbar(rep_dim,
                        np_deepstrom_kernel_k_lin_mean_accuracy_test,
                        np_deepstrom_kernel_k_lin_std_accuracy_test,
                        color=kernel_color[k_name + "_lin"],
                        marker=kernel_marker[k_name + "_lin"],
                        label="Nystrom linear; kernel: {}".format(k_name),
                        linestyle='None',
                        capsize=capsize)
            # get the results with the non linear activation function
            df_deepstrom_kernel_k_nonlin = df_deepstrom_kernel_k[df_deepstrom_kernel_k["non_linearity"] != str(None)]
            np_deepstrom_kernel_k_nonlin_mean_accuracy_test = np.mean(
                np.array(
                    [list(df_deepstrom_kernel_k_nonlin[df_deepstrom_kernel_k_nonlin["seed"] == seed_v]["accuracy_test"])
                     for seed_v in
                     seed_values]), axis=0)
            np_deepstrom_kernel_k_nonlin_std_accuracy_test = np.std(
                np.array(
                    [list(df_deepstrom_kernel_k_nonlin[df_deepstrom_kernel_k_nonlin["seed"] == seed_v]["accuracy_test"])
                     for seed_v in
                     seed_values]), axis=0)
            plt.errorbar(rep_dim,
                        np_deepstrom_kernel_k_nonlin_mean_accuracy_test,
                        np_deepstrom_kernel_k_nonlin_std_accuracy_test,
                        color=kernel_color[k_name + "_nonlin"],
                        marker=kernel_marker[k_name + "_nonlin"],
                        linestyle='None',
                        label="Nystrom non linear; kernel: {}".format(k_name),
                        capsize=capsize)
        # plot dense
        # ==========
        df_dense = df[df["method_name"] == "dense"]
        df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
        df_dense = df_dense.sort_values(by=["repr_dim"])
        np_dense_mean_accuracy_test = np.mean(
            np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        np_dense_std_accuracy_test = np.std(
            np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        plt.errorbar(sorted([int(n) for n in repr_dim]),
                    np_dense_mean_accuracy_test,
                    np_dense_std_accuracy_test,
                    color=dense_color,
                    marker=dense_marker,
                    linestyle='None',
                    label="Dense",
                    capsize=capsize)

        # plot deepfried
        # ==============
        df_deepfried = df[df["method_name"] == "deepfriedconvnet"]
        np_deepfried_mean_accuracy_test = np.mean(
            np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        np_deepfried_std_accuracy_test = np.std(
            np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        plt.errorbar(sorted([int(n) for n in repr_dim]),
                    [np_deepfried_mean_accuracy_test[0] for _ in repr_dim],
                    # [np_deepfried_std_accuracy_test[0] for _ in repr_dim],
                    color=deepfried_color,
                    label="Deepfried Convnet (Repr. Dim = 512 * 2)",
                    capsize=capsize)
        plt.title("Accuracy by repr space dim for \n "
                  "batch size = {} and Nystroem subsample size = {}".format(batch_size, nys_size))
        plt.ylabel("Accuracy")
        plt.xlabel("symlog(Representation space dim)")
        plt.xscale("symlog")
        # plt.ylim(min_acc, max_acc)
        plt.xticks(sorted([int(n) for n in repr_dim]))
        plt.legend()
        plt.show()