import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pathlib

from skluc.main.utils import logger

matplotlib.rcParams.update({'font.size': 14})

# pd.set_option('display.width', 1000)
pd.set_option('display.expand_frame_repr', False)

# DAT = ["SVHN"]
# DIR = ["/home/luc/Resultats/Deepstrom/october_2018/transfert_few_data_cifar100_from_cifar10"]


DATANAME = "CIFAR10"
DIRNAME = "/home/luc/Resultats/Deepstrom/october_2018/transfert_few_data_cifar10_from_cifar100"

FILENAME = "gathered_results.csv"

min_acc = 0.00
max_acc = 1.05
# max_acc = 1.0
linewidth = 0.9
output_conv_dims = {'block3_pool': 256 * 16, 'block5_conv4': 512 * 4, 'block5_pool': 512}
nb_classes = 10

real_nys_marker = "s"

learned_nys_marker = "x"

linearity_color = "g"

dense_marker = "v"
dense_color = "r"

deepfried_marker = "8"
deepfried_color = "b"

d_translate_kernel = {
    "linear": "Linear",
    "chi2_cpd": "Chi2",
    "rbf": "Gaussian"
}

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime_train",
                   "runtime_val",
                   "runtime_test",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "second_layer_size",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "act",
                   "non_linearity",
                   "real_nystrom",
                   "repr_quality",
                   "train_size",
                   "dropout",
                   "dataset",
                   "real_deepfried",
                   "weights"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    df = df[df["accuracy_val"] != 'None']
    df = df.apply(pd.to_numeric, errors="ignore")
    df = df.drop_duplicates()
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    # kernel_names.remove("laplacian")
    repr_dim = set(df["repr_dim"].values)
    repr_dim.remove("None")  # dtype: str
    # repr_dim.remove("16")
    nys_size = set(df["subsample_size"].values)
    nys_size.remove("None")
    nb_layers_deepfried = set(df["nb_layer_deepfried"].values)
    nb_layers_deepfried.remove("None")
    seed_values = set(df["seed"].values)
    batch_size = 128
    train_sizes = set(df["train_size"])

    cut_layers = set(df["repr_quality"].values)

    weights = set(df["weights"].values)

    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))
    logger.debug(f"Tested cut layers: {cut_layers}")

    means_deepstrom = {}

    for weight in weights:
        df_weight = df[df["weights"] == weight]
        for t_size in sorted(list(train_sizes)):
            df_tsize = df_weight[df_weight["train_size"] == t_size]

            for cut_layer in cut_layers:
                df_cut_layer = df_tsize[df_tsize["repr_quality"] == cut_layer]

                # plot deepstrom
                # ==============
                df_deepstrom = df_cut_layer[df_cut_layer["method_name"] == "deepstrom"]
                df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
                df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
                for k_name in sorted(kernel_names):
                    df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]

                    f, ax = plt.subplots()

                    # get the results of learned nystrom
                    df_deepstrom_kernel_w = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"] == False]
                    all_accs_w = np.array([
                        list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for
                        seed_v in seed_values
                    ])
                    np_deepstrom_kernel_w_mean_accuracy_test = np.mean(all_accs_w, axis=0)
                    np_deepstrom_kernel_w_std_accuracy_test = np.std(all_accs_w, axis=0)
                    np_param_nbr_deepstrom_kernel_w = (
                            np.square(np.array(sorted(set(df_deepstrom_kernel_w["subsample_size"])))) +  # m x m
                            # np.array(
                            #     sorted(set(df_deepstrom_kernel_w["subsample_size"]))) * output_conv_dims[cut_layer] +  # m x d
                            np.array(
                                sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * nb_classes)  # m x c

                    ax.errorbar(np_param_nbr_deepstrom_kernel_w,
                                np_deepstrom_kernel_w_mean_accuracy_test,
                                np_deepstrom_kernel_w_std_accuracy_test,
                                marker=learned_nys_marker, color=linearity_color,
                                label="Adaptative Deepström",
                                capsize=3)

                    # get the results of vanilla nystrom
                    df_deepstrom_kernel_k = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"]]
                    if len(df_deepstrom_kernel_k):
                        all_accs_k = np.array([
                            list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for
                            seed_v in seed_values
                        ])
                        np_deepstrom_kernel_k_mean_accuracy_test = np.mean(all_accs_k, axis=0)
                        np_deepstrom_kernel_k_std_accuracy_test = np.std(all_accs_k, axis=0)

                        np_param_nbr_deepstrom_kernel_k = (
                                np.square(np.array(sorted(set(df_deepstrom_kernel_k["subsample_size"])))) +  # m x m
                                # np.array(sorted(
                                #     set(df_deepstrom_kernel_k["subsample_size"]))) * output_conv_dims[cut_layer] +  # m x d
                                np.array(sorted(
                                    list(set(df_deepstrom_kernel_k["subsample_size"])))) * nb_classes)  # m x c

                        ax.errorbar(np_param_nbr_deepstrom_kernel_k,
                                    np_deepstrom_kernel_k_mean_accuracy_test,
                                    np_deepstrom_kernel_k_std_accuracy_test,
                                    marker=real_nys_marker, color=linearity_color,
                                    label="Deepström",
                                    capsize=3)

                    # plot dense
                    # ==========
                    df_dense = df_cut_layer[df_cut_layer["method_name"] == "dense"]
                    df_dense = df_dense[df_dense["train_size"] == t_size]
                    df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
                    df_dense = df_dense.sort_values(by=["repr_dim"])
                    np_dense_mean_accuracy_test = np.mean(
                        np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)
                    np_dense_std_accuracy_test = np.std(
                        np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)
                    ax.errorbar(
                        np.array(sorted([int(n) for n in np.unique(df_dense["repr_dim"])])) * output_conv_dims[cut_layer] +
                        np.array(sorted([int(n) for n in np.unique(df_dense["repr_dim"])])) * nb_classes,
                        np_dense_mean_accuracy_test,
                        np_dense_std_accuracy_test,
                        color=dense_color,
                        marker=dense_marker,
                        label="Fully Connected", capsize=3)

                    # # plot deepfried
                    # # ==============
                    df_deepfried = df_cut_layer[df_cut_layer["method_name"] == "deepfriedconvnet"]
                    np_deepfried_mean_accuracy_test = []
                    np_deepfried_std_accuracy_test = []
                    for l_nb in sorted(nb_layers_deepfried):
                        df_deepfried_stack = df_deepfried[df_deepfried["nb_layer_deepfried"] == l_nb]
                        if len(df_deepfried_stack):
                            np_deepfried_mean_accuracy_test.append(np.mean(df_deepfried_stack["accuracy_test"]))
                            np_deepfried_std_accuracy_test.append(np.std(df_deepfried_stack["accuracy_test"]))

                    nb_param_vals = [(output_conv_dims[cut_layer] * 3 + output_conv_dims[cut_layer] * nb_classes) * int(i) for i in sorted(set(df_deepfried["nb_layer_deepfried"].values))]
                    ax.errorbar(nb_param_vals,
                                np_deepfried_mean_accuracy_test,
                                np_deepfried_std_accuracy_test,
                                color=deepfried_color,
                                marker=deepfried_marker,
                                label="Adaptative DeepFriedConvnet", capsize=3)

                    ax.set_ylim(min_acc, max_acc)
                    ax.set_ylabel("Accuracy")
                    ax.set_xticks([1e4, 1e5, 1e6])
                    # if i == 2:
                    # ax.set_xlabel("# Parameters")
                    ax.set_xlabel("# Learnable Parameters")
                    ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=2)
                    ax.set_xticklabels([1e4, 1e5, 1e6])
                    # else:
                    #     ax.set_xticklabels([])
                    ax.set_xscale("symlog")

                    ax_twin = ax.twiny()
                    ax_twin.set_xscale("symlog")
                    ax_twin.set_xlim(ax.get_xlim())
                    ax_twin.set_xticks(np_param_nbr_deepstrom_kernel_w)

                    # if i == 0:
                    ax_twin.set_xlabel("Subsample Size")
                    ax.set_title(
                        "{} Kernel - {} - Train size: {} - weights: {}".format(d_translate_kernel[k_name], DATANAME, t_size, weight),
                        y=1.2)
                    ax_twin.set_xticklabels(sorted(set(df_deepstrom_kernel_w["subsample_size"])))
                    # else:
                    #     ax.set_title("Noyau {} - {} - Train size: {}".format(d_translate_kernel[k_name], DATANAME, t_size))
                    #     ax_twin.set_xticklabels([])

                    f.set_size_inches(8, 6)
                    f.tight_layout()
                    f.subplots_adjust(bottom=0.3)
                    # f.show()
                    # exit()
                    # learnable: change legend
                    # ODIR = [
                    #     "/home/luc/PycharmProjects/deepFriedConvnets/main/experiments/graph_drawing/paper/svhn/few_data/parameters/dropout_{}".format(
                    #         str(drop_val).replace(".", "-"))]
                    # out_dir_path = ODIR[h]

                    out_name = "acc_param_tsize_{}_{}_{}".format(t_size, cut_layer, k_name)

                    base_out_dir = os.path.join(os.path.abspath(__file__.split(".")[0]), "images", f"{weight}", f"{cut_layer}")
                    pathlib.Path(base_out_dir).mkdir(parents=True, exist_ok=True)
                    out_path = os.path.join(base_out_dir, out_name)
                    logger.debug(out_path)
                    f.savefig(out_path)
