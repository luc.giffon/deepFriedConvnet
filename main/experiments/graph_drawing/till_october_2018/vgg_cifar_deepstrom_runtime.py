import os
import pandas as pd

pd.set_option('display.width', 1000)

DIRNAME = ["/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/april_2018/big_grid_vgg_cifar10_3_big_subsample",
           "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/april_2018/big_grid_vgg_cifar10_3_little_subsample"]
FILENAME = "gathered_results.csv"

min_acc = 0.88
max_acc = 0.93
linewidth = 0.9
capsize = 3
output_conv_dim = 512
kernel_marker = {
    "chi2_cpd": "x",
    "rbf": "d",
    # "summed": "h",
    # "laplacian": "v"
}
kernel_color = {
    "chi2_cpd": "b",
    "rbf": "c",
    # "laplacian": "r",
    # "summed": "m"
}

dense_marker = "+"

dense_color = "k"

deepfried_marker = "8"

deepfried_color = "#F289E7"

field_names = ["method_name",
               "accuracy_val",
               "accuracy_test",
               "runtime",
               "number_epoch",
               "batch_size",
               "repr_dim",
               "two_layers_dense",
               "kernel_deepstrom",
               "gamma_kernel",
               "constante_sigmoid",
               "nb_layer_deepfried",
               "subsample_size",
               "validation_size",
               "seed",
               "non_linearity",
               "real_nystrom"
               ]

if __name__ == '__main__':
    for dir_name in DIRNAME:
        filepath = os.path.join(DIRNAME, FILENAME)
        df = pd.read_csv(filepath, names=field_names)
        df = df[df["accuracy_val"] != str(None)]
        df = df.apply(pd.to_numeric, errors="ignore")





