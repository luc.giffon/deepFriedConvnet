
# coding: utf-8

# In[1]:


import os
import pathlib

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from skluc.main.utils import logger


pd.set_option('display.width', 1000)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('display.max_columns', None)

flatten = lambda l: [item for sublist in l for item in sublist]


# In[3]:


FILENAME = "gathered_results.csv"
DIRNAME = "/home/luc/Resultats/Deepstrom/september_2018/few_shot_tests_knn_nobacktrain/"
filepath = os.path.join(DIRNAME, FILENAME)


# In[5]:


df_root = pd.read_csv(filepath)
df_root = df_root.dropna(how="all")
df_root['--nys-size'].replace('None', np.nan, inplace=True)
df_root['--out-dim'].replace('None', np.nan, inplace=True)
df_root = df_root.apply(pd.to_numeric, errors="ignore")
df_root.head()


# In[7]:


df = df_root.loc[:, ["accuracy_test", "global_time", "--distance", "--nb-neighbour", "--classification-method", "--num-class-ep-test", "--num-class-ep-train", "--num-epoch", "--num-query-ex", "--num-supp-ex", "--nys-size", "--out-dim", "kernel", "network"]]
df.head()


# In[8]:


df.sort_values(by="accuracy_test", ascending=False)


# In[15]:


kernel_names = set(df["kernel"].values)
kernel_names.remove("None")
logger.debug("Kernel functions are: {}".format(kernel_names))

nystrom_sizes = set(df["--nys-size"])
logger.debug(f"Nystrom sizes: {nystrom_sizes}")

output_dims = set(df["--out-dim"])
logger.debug(f"Output dimension: {output_dims}")

networks = set(df["network"].values)
logger.debug(f"Network types are {networks}")

nb_neighbours = set(df["--nb-neighbour"].values)
logger.debug(f"Nb neighbours values are {nb_neighbours}")

distances = set(df["--distance"].values)
logger.debug(f"Possible distances are {distances}")


# In[30]:


step_between_groups = 0.25
group_size = 10
step_inside_groups = 0.15
bar_width = 0.2
min_acc = 0.8
max_acc = 1.0
linewidth = 0.9

real_nys_marker = "s"

learned_nys_marker = "x"

dense_marker = "v"
dense_color = "r"

deepfried_marker = "8"
deepfried_color = "b"

d_translate_kernel = {
    "linear": "Linear",
    "chi2_cpd": "Chi2",
    "rbf": "Gaussian",
}


for idx_plot, n_size in enumerate(sorted(nystrom_sizes)):
    if np.isnan(n_size):
        continue
    fig, axs = plt.subplots()
    group_of_bins = []
    group_of_bins_labels = []
    
    
    df_none = df[df["network"] == "none"]
    current_group_of_bins = []
    current_group_of_bins_labels = []
    for nb_neigh in nb_neighbours:
        df_neigh = df_none[df_none["--nb-neighbour"] == nb_neigh]
        for dist in distances:
            df_dist = df_neigh[df_neigh["--distance"] == dist]
            current_group_of_bins.append(df_dist["accuracy_test"].values[0])
            current_group_of_bins_labels.append(f"No repr-{dist}-{nb_neigh}")
    group_of_bins.append(current_group_of_bins)
    group_of_bins_labels.append(current_group_of_bins_labels)
    df_deepstrom = df[df["network"] == "deepstrom"]
        
    df_n_size = df_deepstrom[df_deepstrom["--nys-size"] == n_size]

    for o_dim in sorted(output_dims):
        if np.isnan(o_dim):
            continue
        df_o_dim = df_n_size[df_n_size["--out-dim"] == o_dim]
        if len(df_o_dim) > 0:
            current_group_of_bins = []
            current_group_of_bins_labels = []
            for k_name in sorted(kernel_names):
                df_k_name = df_o_dim[df_o_dim["kernel"] == k_name]
                for nb_neigh in nb_neighbours:
                    df_neigh = df_k_name[df_k_name["--nb-neighbour"] == nb_neigh]
                    for dist in distances:
                        df_dist = df_neigh[df_neigh["--distance"] == dist]
                        current_group_of_bins.append(df_dist["accuracy_test"].values[0])
                        current_group_of_bins_labels.append(f"{k_name}-{int(o_dim)}-{dist}-{nb_neigh}")

            group_of_bins.append(current_group_of_bins)
            group_of_bins_labels.append(current_group_of_bins_labels)
    
    x_locations_total = []
    x_labels_total = []
    for i, group in enumerate(group_of_bins):
        x_locations = np.linspace(start=i*(group_size) + step_between_groups, stop=(i+1)*(group_size) - step_between_groups, num=len(group))
        x_locations_total.extend(list(x_locations))
        x_labels_total.extend(list(group_of_bins_labels[i]))
        axs.bar(x_locations, 
                group,
                width=bar_width,
                align="edge")
    axs.set_ylim(min_acc, max_acc)        
    axs.set_ylabel('Accuracies')
    title = f'Deepstrom - omniglot - size {int(n_size)} - few shot - big subsample'
    axs.set_title(title)
    plt.xticks(x_locations_total, x_labels_total, rotation=75)

    base_out_dir = os.path.join(os.path.abspath(__file__.split(".")[0]), "images")
    out_name = title.replace(" - ", "_")
    pathlib.Path(base_out_dir).mkdir(parents=True, exist_ok=True)
    out_path = os.path.join(base_out_dir, out_name)

    # plt.set_size_inches(8, 6)
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.35)
    plt.savefig(out_path)

    plt.show()

# plt.xticks(index + bar_width / 2)
# ax.set_xticklabels(('A', 'B', 'C', 'D', 'E'))
# ax.legend()
# plt.bar(np.arange(4), np.random.rand(4))

