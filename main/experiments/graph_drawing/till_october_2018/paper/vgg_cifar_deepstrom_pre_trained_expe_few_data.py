import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger
import matplotlib

matplotlib.rcParams.update({'font.size': 14})

pd.set_option('display.width', 1000)

# DATANAME = "CIFAR"
# DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/may_2018/benchmark_cifar_few_data_more_subsample_gathered"
# DATANAME = "MNIST"
# DIRNAME = "/home/luc/Resultats/Deepstrom/MNIST/may_2018/benchmark_mnist_few_data_more_subsample_gathered"
DAT = [
"CIFAR",
"MNIST"
]
DIR = [
"/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/may_2018/benchmark_cifar_few_data_more_subsample_gathered",
"/home/luc/Resultats/Deepstrom/MNIST/may_2018/benchmark_mnist_few_data_more_subsample_gathered"
]
# ODIR = [
# "/home/luc/PycharmProjects/deepFriedConvnets/main/experiments/graph_drawing/paper/cifar/few_data_more_subsample/learnable_parameters",
# "/home/luc/PycharmProjects/deepFriedConvnets/main/experiments/graph_drawing/paper/mnist/few_data_more_subsample/learnable_parameters",
# ]
ODIR = [
"/home/luc/PycharmProjects/deepFriedConvnets/main/experiments/graph_drawing/paper/cifar/few_data_more_subsample/parameters",
"/home/luc/PycharmProjects/deepFriedConvnets/main/experiments/graph_drawing/paper/mnist/few_data_more_subsample/parameters",
]

for h in range(len(DIR)):
    DATANAME = DAT[h]
    DIRNAME = DIR[h]

    FILENAME = "gathered_results.csv"

    min_acc = 0.00
    max_acc = 1.05
    # max_acc = 1.0
    linewidth = 0.9
    output_conv_dim = 512
    nb_classes = 10

    real_nys_marker = "s"

    learned_nys_marker = "x"

    linearity_color = "g"

    dense_marker = "v"
    dense_color = "r"

    deepfried_marker = "8"
    deepfried_color = "b"

    d_translate_kernel = {
        "linear": "Linear",
        "chi2_cpd": "Chi2",
        "rbf": "Gaussian"
    }

    if __name__ == '__main__':
        filepath = os.path.join(DIRNAME, FILENAME)
        field_names = ["method_name",
                       "accuracy_val",
                       "accuracy_test",
                       "runtime",
                       "number_epoch",
                       "batch_size",
                       "repr_dim",
                       "two_layers_dense",
                       "kernel_deepstrom",
                       "gamma_kernel",
                       "constante_sigmoid",
                       "nb_layer_deepfried",
                       "subsample_size",
                       "validation_size",
                       "seed",
                       # "non_linearity",
                       "real_nystrom",
                       "repr_quality",
                       "train_size"
                       ]

        df = pd.read_csv(filepath, names=field_names)
        # df = df[df["accuracy_val"] != 'None']
        df = df.apply(pd.to_numeric, errors="ignore")
        method_names = set(df["method_name"].values)
        kernel_names = set(df["kernel_deepstrom"].values)
        kernel_names.remove("None")
        # kernel_names.remove("laplacian")
        repr_dim = set(df["repr_dim"].values)
        repr_dim.remove("None")  # dtype: str
        # repr_dim.remove("16")
        nys_size = set(df["subsample_size"].values)
        nys_size.remove("None")
        nb_layers_deepfried = set(df["nb_layer_deepfried"].values)
        nb_layers_deepfried.remove("None")
        seed_values = set(df["seed"].values)
        batch_size = 128
        train_sizes = set(df["train_size"])
        logger.debug("Nystrom possible sizes are: {}".format(nys_size))
        logger.debug("Kernel functions are: {}".format(kernel_names))
        logger.debug("Compared network types are: {}".format(method_names))
        logger.debug("Tested representation dimension are: {}".format(repr_dim))

        means_deepstrom = {}

        for t_size in sorted(list(train_sizes)):


            df_tsize = df[df["train_size"] == t_size]
            # plot deepstrom
            # ==============
            df_deepstrom = df_tsize[df_tsize["method_name"] == "deepstrom"]
            df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
            df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
            for i, k_name in enumerate(sorted(kernel_names)):
                f, ax = plt.subplots()
                df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]
                # non_lin_dfs = {
                #     "linear": df_deepstrom_kernel[df_deepstrom_kernel["non_linearity"] == "None"],
                # }
                # get the results of learned nystrom
                df_deepstrom_kernel_w = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"] == False]
                np_deepstrom_kernel_w_mean_accuracy_test = np.mean(np.array([
                    list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in seed_values
                ]), axis=0)
                np_deepstrom_kernel_w_std_accuracy_test = np.std(np.array([list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                                                           seed_values]), axis=0)
                np_param_nbr_deepstrom_kernel_w = (
                        np.square(np.array(sorted(set(df_deepstrom_kernel_w["subsample_size"])))) +  # m x m
                        np.array(sorted(set(df_deepstrom_kernel_w["subsample_size"]))) * output_conv_dim +  # m x d
                        np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * nb_classes)  # m x c

                ax.errorbar(np_param_nbr_deepstrom_kernel_w,
                               np_deepstrom_kernel_w_mean_accuracy_test,
                               np_deepstrom_kernel_w_std_accuracy_test,
                               marker=learned_nys_marker, color=linearity_color,
                               label=" Adaptive-$\phi_{nys}$",
                               linestyle="None", capsize=3)

                # get the results of vanilla nystrom
                df_deepstrom_kernel_k = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"]]
                if len(df_deepstrom_kernel_k):
                    np_deepstrom_kernel_k_mean_accuracy_test = np.mean(
                        np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)
                    np_deepstrom_kernel_k_std_accuracy_test = np.std(
                        np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)

                    np_param_nbr_deepstrom_kernel_k = (
                            np.square(np.array(sorted(set(df_deepstrom_kernel_k["subsample_size"])))) +  # m x m
                            np.array(sorted(set(df_deepstrom_kernel_k["subsample_size"]))) * output_conv_dim +  # m x d
                            np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"])))) * nb_classes)  # m x c

                    ax.errorbar(np_param_nbr_deepstrom_kernel_k,
                                   np_deepstrom_kernel_k_mean_accuracy_test,
                                   np_deepstrom_kernel_k_std_accuracy_test,
                                   marker=real_nys_marker, color=linearity_color,
                                   label="$\phi_{nys}$",
                                   linestyle="None", capsize=3)

                # plot dense
                # ==========
                df_dense = df_tsize[df_tsize["method_name"] == "dense"]
                df_dense = df_dense[df_dense["train_size"] == t_size]
                df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
                df_dense = df_dense.sort_values(by=["repr_dim"])
                np_dense_mean_accuracy_test = np.mean(
                    np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                              seed_values]), axis=0)
                np_dense_std_accuracy_test = np.std(
                    np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                              seed_values]), axis=0)
                ax.errorbar(np.array(sorted([int(n) for n in np.unique(df_dense["repr_dim"])])) * output_conv_dim +
                               np.array(sorted([int(n) for n in np.unique(df_dense["repr_dim"])])) * nb_classes,
                               np_dense_mean_accuracy_test,
                               np_dense_std_accuracy_test,
                               color=dense_color,
                               marker=dense_marker,
                               label="$\phi_{nn}$", capsize=3, linestyle="None")

                # plot deepfried
                # ==============
                df_deepfried = df_tsize[df_tsize["method_name"] == "deepfriedconvnet"]
                np_deepfried_mean_accuracy_test = []
                np_deepfried_std_accuracy_test = []
                for l_nb in sorted(nb_layers_deepfried):
                    df_deepfried_stack = df_deepfried[df_deepfried["nb_layer_deepfried"] == l_nb]
                    np_deepfried_mean_accuracy_test.append(np.mean(df_deepfried_stack["accuracy_test"]))
                    np_deepfried_std_accuracy_test.append(np.std(df_deepfried_stack["accuracy_test"]))

                ax.errorbar([(output_conv_dim * 3 + output_conv_dim * nb_classes) * i for i in [1, 2, 3, 8]],
                               np_deepfried_mean_accuracy_test,
                               np_deepfried_std_accuracy_test,
                               color=deepfried_color,
                               marker=deepfried_marker,
                               linestyle="None",
                               label=" Adaptive-$\phi_{ff}$", capsize=3)
                ax.set_ylim(min_acc, max_acc)
                ax.set_ylabel("Accuracy")
                ax.set_xticks([1e4, 1e5, 1e6])
                # if i == 2:
                ax.set_xlabel("symlog(# Parameters)")
                # ax.set_xlabel("symlog(# Parameters)")
                ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=4)
                ax.set_xticklabels([1e4, 1e5, 1e6])
                # else:
                #     ax.set_xticklabels([])
                ax.set_xscale("symlog")

                ax_twin = ax.twiny()
                ax_twin.set_xscale("symlog")
                ax_twin.set_xlim(ax.get_xlim())
                ax_twin.set_xticks(np_param_nbr_deepstrom_kernel_w)

                # if i == 0:
                ax_twin.set_xlabel("Subsample Size")
                ax.set_title("{} Kernel - {} - Train size: {}".format(d_translate_kernel[k_name], DATANAME, t_size),
                                y=1.2)
                ax_twin.set_xticklabels(sorted(set(df_deepstrom_kernel_w["subsample_size"])))
                # else:
                #     ax.set_title("Noyau {} - {} - Train size: {}".format(d_translate_kernel[k_name], DATANAME, t_size))
                #     ax_twin.set_xticklabels([])

                f.set_size_inches(8, 6)
                f.tight_layout()
                f.subplots_adjust(bottom=0.3)
                # f.show()
                # exit()
                # learnable: change legend
                out_dir_path = ODIR[h]
                out_name = "acc_param_tsize_{}".format(t_size)
                out_path = os.path.join(out_dir_path, out_name)
                f.savefig(out_path)
