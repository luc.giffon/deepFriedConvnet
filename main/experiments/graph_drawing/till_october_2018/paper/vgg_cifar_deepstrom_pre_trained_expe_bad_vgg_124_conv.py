import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger
import matplotlib

matplotlib.rcParams.update({'font.size': 14})

pd.set_option('display.width', 1000)
# dirnames = ["/home/luc/Resultats/Deepstrom/MNIST/mnist_pretrain_expe_1",
# "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/april_2018/big_grid_vgg_cifar10_pretrain_expe_reassembled"]
# filenames = [
# "gathered_results.csv",
# "gathered_results4.csv"
# ]
# FILENAME = "gathered_results.csv"
# datanames = ["MNIST",
#              "CIFAR10" ]
# DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/april_2018/big_grid_vgg_cifar10_pretrain_expe_reassembled"
# FILENAME = "gathered_results4.csv"

DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/may_2018/benchmark_vggveryverybad_and_vggveryveryverybad"
FILENAME = "gathered_results.csv"

n_blocks = {
    1: 4,
    3: 2,
    4: 1
}

# DIRNAME = dirnames[h]
# FILENAME = filenames[h]
# DATANAME = datanames[h]
min_acc = 0.00
max_acc = 1.05
# max_acc = 1.0
linewidth = 0.9
output_conv_dim = 512
nb_classes = 10

real_nys_marker = "s"

learned_nys_marker = "x"

linearity_color={
    "linear": "m",
    "non-linear": "y"
}

# kernel_marker = {
#     "chi2_cpd": "x",
#     "rbf": "d",
#     "linear": "h",
# }
# kernel_color = {
#     "chi2_cpd": "b",
#     "linear": "r",
#     "rbf": "c",
# }

dense_marker = "v"
dense_color = "#A2EF3E"

deepfried_marker = "8"
deepfried_color = "#F289E7"

d_translate_kernel = {
    "linear": "Linéaire",
    "chi2_cpd": "Chi2",
    "rbf": "Gaussien"
}

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom",
                   "removed_block"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    df = df.apply(pd.to_numeric, errors="ignore")
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    # kernel_names.remove("laplacian")
    repr_dim = set(df["repr_dim"].values)
    # repr_dim.remove("None")  # dtype: str
    # repr_dim.remove("16")
    nys_size = set(df["subsample_size"].values)
    nys_size.remove("None")
    seed_values = set(df["seed"].values)
    batch_size = 128
    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))
    for removed_block in [1, 3, 4]:
        DATANAME = "CIFAR10 - {} conv block".format(n_blocks[removed_block])

        means_deepstrom = {}
        df_b = df[df["removed_block"] == removed_block]
        # plot deepstrom
        # ==============
        df_deepstrom = df_b[df_b["method_name"] == "deepstrom"]
        df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
        df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
        f, ax = plt.subplots(3, 1, sharey = True, sharex = True)
        for i, k_name in enumerate(kernel_names):
            df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]
            non_lin_dfs = {
                "linear": df_deepstrom_kernel[df_deepstrom_kernel["non_linearity"] == "None"],
            }
            # get the results of learned nystrom
            for linearity, df_deepstrom_kernel_lin_case in non_lin_dfs.items():
                df_deepstrom_kernel_w = df_deepstrom_kernel_lin_case[df_deepstrom_kernel_lin_case["real_nystrom"] == False]
                np_deepstrom_kernel_w_mean_accuracy_test = np.mean(np.array([
                    list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in seed_values
                ]), axis=0)
                np_deepstrom_kernel_w_std_accuracy_test = np.std(np.array([list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                                                           seed_values]), axis=0)
                # get the results of vanilla nystrom
                df_deepstrom_kernel_k = df_deepstrom_kernel_lin_case[df_deepstrom_kernel_lin_case["real_nystrom"]]
                if len(df_deepstrom_kernel_k):
                    np_deepstrom_kernel_k_mean_accuracy_test = np.mean(
                        np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)
                    np_deepstrom_kernel_k_std_accuracy_test = np.std(
                        np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)

                    ax[i].errorbar(np.square(np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"]))))) +
                                   np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"])))) * 10,
                                   np_deepstrom_kernel_k_mean_accuracy_test,
                                   np_deepstrom_kernel_k_std_accuracy_test,
                                   marker=real_nys_marker, color=linearity_color[linearity], label="$\phi_{nys}$" + (" + relu" if linearity == "linear" else ""), linestyle="None", capsize=3)
                    # ax[i].legend(bbox_to_anchor=(0.065, -0.5), loc="lower left", ncol=3)
                    # ax[i][h].errorbar(np.square(np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))))) +
                    #                np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * 10 +
                    #                output_conv_dim * np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))),
                    #                np_deepstrom_kernel_k_mean_accuracy_test,
                    #                np_deepstrom_kernel_k_std_accuracy_test,
                    #                marker=real_nys_marker, color=linearity_color[linearity], label="$\phi_{nys}$" + (" + relu" if linearity == "linear" else ""), linestyle="None", capsize=3)
                    #
                ax[i].errorbar(np.square(np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))))) +
                               np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * 10,
                               np_deepstrom_kernel_w_mean_accuracy_test,
                               np_deepstrom_kernel_w_std_accuracy_test,
                               marker=learned_nys_marker, color=linearity_color[linearity], label="Adaptative-$\phi_{nys}$" + (" + relu" if linearity == "linear" else ""), linestyle="None", capsize=3)
                #         #
                # ax[i][h].errorbar(np.square(np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))))) +
                #                np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * 10 +
                #                output_conv_dim * np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))),
                #                np_deepstrom_kernel_w_mean_accuracy_test,
                #                np_deepstrom_kernel_w_std_accuracy_test,
                #                marker=learned_nys_marker, color=linearity_color[linearity], label="Adaptative-$\phi_{nys}$" + (" + relu" if linearity == "linear" else ""), linestyle="None", capsize=3)
                #
            # plot dense
            # ==========
            df_dense = df_b[df_b["method_name"] == "dense"]
            df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
            df_dense = df_dense.sort_values(by=["repr_dim"])
            np_dense_mean_accuracy_test = np.mean(
                np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            np_dense_std_accuracy_test = np.std(
                np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            ax[i].errorbar(np.array(sorted([int(n) for n in repr_dim])) * output_conv_dim +
                           np.array(sorted([int(n) for n in repr_dim])) * nb_classes,
                           np_dense_mean_accuracy_test,
                           np_dense_std_accuracy_test,
                           color=dense_color,
                           marker=dense_marker,
                           label="$\phi_{nn}$", capsize=3, linestyle="None")
            # plt.xscale("symlog")
            # plt.legend()
            # plt.show()
            # plot deepfried
            # ==============
            # df_deepfried = df_b[df_b["method_name"] == "deepfriedconvnet"]
            # np_deepfried_mean_accuracy_test = np.mean(
            #     np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
            #               seed_values]), axis=0)
            # np_deepfried_std_accuracy_test = np.std(
            #     np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
            #               seed_values]), axis=0)
            # print(np_deepfried_mean_accuracy_test, np_deepfried_std_accuracy_test)
            # ax[i].errorbar(output_conv_dim * 3 + output_conv_dim * nb_classes,
            #                np_deepfried_mean_accuracy_test,
            #                np_deepfried_std_accuracy_test,
            #                color=deepfried_color,
            #                marker=deepfried_marker,
            #                linestyle = "None",
            #                label="Adaptative-$\phi_{ff}$", capsize=3)
            ax[i].set_ylim(min_acc, max_acc)
            ax[i].set_ylabel("Précision")
            if i == 2:
                # handles, labels = ax[i].get_legend_handles_labels()
                # handles = [h[0] for h in handles]
                ax[i].set_xlabel("symlog(nb paramètres apprenables)")
                # ax[i].legend(handles, labels, bbox_to_anchor=(0.065, -0.5), loc="lower left", ncol=3)
                ax[i].legend(bbox_to_anchor=(0.065, -0.5), loc="lower left", ncol=2)
            ax[i].set_xscale("symlog")
            ax[i].set_title("Noyau {} - {}".format(d_translate_kernel[k_name], DATANAME))
        l_dense = []
        l_nys = []
        print(DATANAME)
        print("dense")
        for i, r_dim in enumerate(sorted([int(n) for n in repr_dim])):
            df_dense = df_b[df_b["method_name"] == "dense"]
            df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
            df_dense = df_dense.sort_values(by=["repr_dim"])
            np_dense_mean_accuracy_test = np.mean(
                np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)

            print(r_dim, np_dense_mean_accuracy_test[i])
            l_dense.append(np_dense_mean_accuracy_test[i])
        print()
        # print("deepfried")
        # print(np_deepfried_mean_accuracy_test[0])
        # print()
        print("deepstrom")
        df_deepstrom = df_b[df_b["method_name"] == "deepstrom"]
        df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
        df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
        for i, r_dim in enumerate(sorted([int(n) for n in nys_size])):
            df_deepstrom_size = df_deepstrom_sort[df_deepstrom_sort["subsample_size"] == r_dim]
            np_deepstrom_mean = np.mean(np.array([
                list(df_deepstrom_size[df_deepstrom_size["seed"] == seed_v]["accuracy_test"]) for seed_v in
                seed_values
            ]), axis=0)
            print(r_dim, max(np_deepstrom_mean))
            l_nys.append(max(np_deepstrom_mean))

        print("dense2")
        print(" ".join(["%.2f" % (x * 100) for x in l_dense]))
        print("nys2")
        print(" ".join(["%.2f" % (x * 100) for x in l_nys]))
        f.set_size_inches(11, 15.5)
        # f.savefig("acc_param")
        f.show()
    # plt.xticks(sorted([int(n) for n in nys_size]))
