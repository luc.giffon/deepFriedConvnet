import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger
import matplotlib

matplotlib.rcParams.update({'font.size': 12})

pd.set_option('display.width', 1000)

DATANAME = "CIFAR10"
DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/may_2018/benchmark_reconstructed_stacked_summed_normal_various_quality_data"
FILENAME = "gathered_results2.csv"

min_acc = 0.00
max_acc = 1.05
# max_acc = 1.0
linewidth = 0.9
output_conv_dim = 512
nb_classes = 10


real_nys_color_by_kernel = {
    "stacked": "c",
    "summed": "pink",
    "rbf": "orange"
}

learned_nys_color_by_kernel = {
    "stacked": "purple",
    "summed": "m",
    "rbf": "g"
}


# learned_euristic_color = "g"
# real_euristic_color = "#A2EF3E"

d_translate_kernel = {
    "stacked": "Stacked",
    "summed": "Summed",
    "rbf": "RBF"
}

d_translate_repr = {
    0: "All (5)",
    1: "4",
    2: "3",
    3: "2",
    'None': "All (5)"
}

bar_width = 0.1

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel1",
                   # "gamma_kernel2",
                   # "gamma_kernel3",
                   # "gamma_kernel4",
                   # "gamma_kernel5",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom",
                   "repr_quality",
                   # "train_size"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    # df = df[df["accuracy_val"] != 'None']
    df = df.loc[(df['repr_quality'] == "None") | (df['repr_quality'] == "0")]
    df = df.apply(pd.to_numeric, errors="ignore")
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    # kernel_names.remove("None")
    # kernel_names.remove("laplacian")
    repr_dim = set(df["repr_dim"].values)
    # repr_dim.remove("None")  # dtype: str
    # repr_dim.remove("16")
    nys_size = set(df["subsample_size"].values)
    # nys_size.remove("None")
    seed_values = set(df["seed"].values)
    batch_size = 128
    # repr_qualities = set(df["repr_quality"])
    g_values = set(df["gamma_kernel1"])
    g_values.remove("None")
    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))

    means_deepstrom = {}

    # for r_qual in sorted(list(repr_qualities)):
    df_rqual = df
    # plot deepstrom
    # ==============
    df_deepstrom = df_rqual[df_rqual["method_name"] == "deepstrom"]
    df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
    df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])

    for n_size in sorted(nys_size):
        f, ax = plt.subplots()
        df_nsize = df_deepstrom_sort[df_deepstrom_sort["subsample_size"] == n_size]
        for i, k_name in enumerate(["rbf"]):
            df_kernel = df_nsize[df_nsize["kernel_deepstrom"] == k_name]

            # get the results of learned nystrom
            df_kernel_w = df_kernel[df_kernel["real_nystrom"] == "False"]
            np_kernel_w_mean_accuracy_test_range = []
            np_kernel_w_std_accuracy_test_range = []
            np_kernel_w_mean_accuracy_test_euristic = []
            np_kernel_w_std_accuracy_test_euristic = []
            j_euristic = None
            for j, g_value in enumerate(reversed(sorted(g_values))):
                df_kernel_w_gval = df_kernel_w[df_kernel_w["gamma_kernel1"] == g_value]
                s_value = 1./float(g_value)
                # if g_value in [str(i) for i in np.logspace(-1, -4, 4)] + ["0.00001"]:
                np_kernel_w_mean_accuracy_test_range.append(np.mean(df_kernel_w_gval["accuracy_test"]))
                np_kernel_w_std_accuracy_test_range.append(np.std(df_kernel_w_gval["accuracy_test"]))
                # else:
                #     np_kernel_w_mean_accuracy_test_range.append(0)
                #     np_kernel_w_std_accuracy_test_range.append(0)
                #     np_kernel_w_mean_accuracy_test_euristic.append(np.mean(df_kernel_w_gval["accuracy_test"]))
                #     np_kernel_w_std_accuracy_test_euristic.append(np.std(df_kernel_w_gval["accuracy_test"]))
                #     j_euristic = j

            # get the results of vanilla nystrom
            ax.bar(np.arange(len(np_kernel_w_mean_accuracy_test_range)) +bar_width*2*i, np_kernel_w_mean_accuracy_test_range,
                   width=bar_width, label=str(d_translate_kernel[k_name]) + " -$\phi_{nys}$",
                   color=learned_nys_color_by_kernel[k_name])
            ax.errorbar(np.arange(len(np_kernel_w_mean_accuracy_test_range)) + bar_width*2*i,
                        np_kernel_w_mean_accuracy_test_range, yerr=np_kernel_w_std_accuracy_test_range,
                        color="black", fmt='none', capsize=3)

            # ax.bar([j_euristic + bar_width * 2 * i],
            #        np_kernel_w_mean_accuracy_test_euristic,
            #        width=bar_width, label=str(d_translate_kernel[k_name]) + " -$\phi_{nys}$",
            #        color=learned_euristic_color)
            # ax.errorbar([j_euristic + bar_width * 2 * i],
            #        np_kernel_w_mean_accuracy_test_euristic, yerr=np_kernel_w_std_accuracy_test_euristic,
            #             color="black", fmt='none', capsize=3)

            # df_kernel_k = df_kernel[df_kernel["real_nystrom"] == "True"]
            # if len(df_kernel_k):
            #     np_kernel_k_mean_accuracy_test_range = []
            #     np_kernel_k_std_accuracy_test_range = []
            #     np_kernel_k_mean_accuracy_test_euristic = []
            #     np_kernel_k_std_accuracy_test_euristic = []
            #     j_euristic = None
            #     for j, g_value in enumerate(sorted(g_values)):
            #         df_kernel_k_gval = df_kernel_k[df_kernel_k["gamma_kernel1"] == g_value]
            #         s_value = 1. / float(g_value)
            #         if g_value in [str(i) for i in np.logspace(-1, -4, 4)] + ["0.00001"]:
            #             np_kernel_k_mean_accuracy_test_range.append(np.mean(df_kernel_k_gval["accuracy_test"]))
            #             np_kernel_k_std_accuracy_test_range.append(np.std(df_kernel_k_gval["accuracy_test"]))
            #
            #         else:
            #             np_kernel_k_mean_accuracy_test_euristic.append(np.mean(df_kernel_k_gval["accuracy_test"]))
            #             np_kernel_k_std_accuracy_test_euristic.append(np.std(df_kernel_k_gval["accuracy_test"]))
            #             np_kernel_k_mean_accuracy_test_range.append(0)
            #             np_kernel_k_std_accuracy_test_range.append(0)
            #             j_euristic = j

                # get the results of vanilla nystrom
                # ax.bar(np.arange(len(np_kernel_k_mean_accuracy_test_range)) + bar_width + bar_width * 2 * i,
                #        np_kernel_k_mean_accuracy_test_range,
                #        width=bar_width, label=str(d_translate_kernel[k_name]) + " $\phi_{nys}$",
                #        color=real_nys_color_by_kernel[k_name])
                # ax.errorbar(np.arange(len(np_kernel_k_mean_accuracy_test_range)) + bar_width + bar_width * 2 * i,
                #             np_kernel_k_mean_accuracy_test_range, yerr=np_kernel_k_std_accuracy_test_range,
                #             color="black", fmt='none', capsize=3)
                #
                # ax.bar([j_euristic + bar_width + bar_width * 2 * i],
                #        np_kernel_k_mean_accuracy_test_euristic,
                #        width=bar_width, label=str(d_translate_kernel[k_name]) + " $\phi_{nys}$",
                #        color=real_euristic_color)
                # ax.errorbar([j_euristic + bar_width + bar_width * 2 * i],
                #             np_kernel_k_mean_accuracy_test_euristic, yerr=np_kernel_k_std_accuracy_test_euristic,
                #             color="black", fmt='none', capsize=3)

            for i, k_name in enumerate(["stacked"]):
                df_kernel = df_nsize[df_nsize["kernel_deepstrom"] == k_name]

                # get the results of learned nystrom
                df_kernel_w = df_kernel[df_kernel["real_nystrom"] == "False"]
                np_kernel_w_mean_accuracy_test = np.mean(df_kernel_w["accuracy_test"])
                np_kernel_w_std_accuracy_test = np.std(df_kernel_w["accuracy_test"])
                xx = np.arange(-2, 10)
                yy = np.array([np_kernel_w_mean_accuracy_test for _ in xx])
                yyerr = np.array([np_kernel_w_std_accuracy_test for _ in xx])
                ax.plot(xx, yy,
                        label=str(d_translate_kernel[k_name]) + " -$\phi_{nys}$",
                        color=learned_nys_color_by_kernel[k_name]
                        )
                ax.fill_between(xx, yy-yyerr, yy+yyerr, facecolor='#bca6f4', alpha=1,
                                edgecolor='purple', linewidth=0.3)

                # df_kernel_k = df_kernel[df_kernel["real_nystrom"] == "True"]
                # np_kernel_k_mean_accuracy_test = np.mean(df_kernel_k["accuracy_test"])
                # np_kernel_k_std_accuracy_test = np.std(df_kernel_k["accuracy_test"])
                # ax.plot(xx, [np_kernel_k_mean_accuracy_test for _ in xx],
                #         label=str(d_translate_kernel[k_name]) + " $\phi_{nys}$",
                #         color=real_nys_color_by_kernel[k_name]
                #         )
        ax.set_ylim(min_acc, max_acc)
        ax.set_xlim(-0.5, 5.5)
        ax.set_ylabel("Accuracy")
        ax.set_xlabel("Sigma Value")
        ax.legend(bbox_to_anchor=(0.5, -0.15), loc="upper center", ncol=3)
        # ax.set_xticks(np.arange(len(nys_size)) + bar_width * len(kernel_names))
        ll = ["0", "1e1", "1e2", "1.843e2", "1e3", "1e4", "1e5"]
        ax.set_xticklabels([str(i) for i in ll])
        # ax.tick_params(length=0)
        ax.set_title("{} - Subsample size: {}".format(DATANAME, n_size))
        f.set_size_inches(5, 5)
        f.subplots_adjust(bottom=0.30)
        # f.show()
        # exit()
        out_dir_path = "/home/luc/PycharmProjects/deepFriedConvnets/" \
                       "main/experiments/graph_drawing/paper/cifar/multikernel/by_sigma_value"
        out_name = "acc_param_nsize_{}".format(n_size)
        out_path = os.path.join(out_dir_path, out_name)
        f.savefig(out_path)
