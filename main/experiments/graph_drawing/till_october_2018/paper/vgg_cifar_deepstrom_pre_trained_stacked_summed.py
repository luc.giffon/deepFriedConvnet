import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger
import matplotlib

matplotlib.rcParams.update({'font.size': 12})

pd.set_option('display.width', 1000)

DATANAME = "CIFAR10"
DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/may_2018/benchmark_reconstructed_stacked_summed_normal_various_quality_data"
FILENAME = "gathered_results.csv"

min_acc = 0.00
max_acc = 1.05
# max_acc = 1.0
linewidth = 0.9
output_conv_dim = 512
nb_classes = 10


real_nys_color_by_kernel = {
    "stacked": "c",
    "summed": "pink",
    "rbf": "orange"
}

learned_nys_color_by_kernel = {
    "stacked": "b",
    "summed": "m",
    "rbf": "r"
}


dense_color = "#A2EF3E"

deepfried_color = "#F289E7"

d_translate_kernel = {
    "stacked": "Stacked",
    "summed": "Summed",
    "rbf": "RBF"
}

d_translate_repr = {
    0: "All (5)",
    1: "4",
    2: "3",
    3: "2",
    'None': "All (5)"
}

bar_width = 0.1

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel1",
                   # "gamma_kernel2",
                   # "gamma_kernel3",
                   # "gamma_kernel4",
                   # "gamma_kernel5",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom",
                   "repr_quality",
                   # "train_size"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    # df = df[df["accuracy_val"] != 'None']
    df = df[df["repr_quality"] != "None"]
    df = df.apply(pd.to_numeric, errors="ignore")

    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    # kernel_names.remove("None")
    # kernel_names.remove("laplacian")
    repr_dim = set(df["repr_dim"].values)
    # repr_dim.remove("None")  # dtype: str
    # repr_dim.remove("16")
    nys_size = set(df["subsample_size"].values)
    # nys_size.remove("None")
    seed_values = set(df["seed"].values)
    batch_size = 128
    repr_qualities = set(df["repr_quality"])
    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))

    means_deepstrom = {}

    for r_qual in sorted(list(repr_qualities)):
        df_rqual = df[df["repr_quality"] == r_qual]
        # plot deepstrom
        # ==============
        df_deepstrom = df_rqual[df_rqual["method_name"] == "deepstrom"]
        df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
        df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
        f, ax = plt.subplots()
        for i, k_name in enumerate(sorted(kernel_names)):
            df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]

            # get the results of learned nystrom
            df_deepstrom_kernel_w = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"] == False]
            np_deepstrom_kernel_w_mean_accuracy_test = []
            np_deepstrom_kernel_w_std_accuracy_test = []
            for n_size in sorted(nys_size):
                deepstrom_kernel_w_nsize = df_deepstrom_kernel_w[df_deepstrom_kernel_w["subsample_size"]==n_size]
                np_deepstrom_kernel_w_mean_accuracy_test.append(np.mean(deepstrom_kernel_w_nsize["accuracy_test"]))
                np_deepstrom_kernel_w_std_accuracy_test.append(np.std(deepstrom_kernel_w_nsize["accuracy_test"]))
            # get the results of vanilla nystrom
            ax.bar(np.arange(len(np_deepstrom_kernel_w_mean_accuracy_test)) +bar_width*2*i, np_deepstrom_kernel_w_mean_accuracy_test,
                   width=bar_width, label=str(d_translate_kernel[k_name]) + " Adaptative-$\phi_{nys}$", color=learned_nys_color_by_kernel[k_name])
            ax.errorbar(np.arange(len(np_deepstrom_kernel_w_mean_accuracy_test)) + bar_width*2*i,
                        np_deepstrom_kernel_w_mean_accuracy_test, yerr=np_deepstrom_kernel_w_std_accuracy_test,
                        color="black", fmt='none', capsize=3)

            df_deepstrom_kernel_k = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"]]
            if len(df_deepstrom_kernel_k):
                np_deepstrom_kernel_k_mean_accuracy_test = []
                np_deepstrom_kernel_k_std_accuracy_test = []
                for n_size in sorted(nys_size):
                    deepstrom_kernel_k_nsize = df_deepstrom_kernel_k[df_deepstrom_kernel_k["subsample_size"] == n_size]
                    np_deepstrom_kernel_k_mean_accuracy_test.append(np.mean(deepstrom_kernel_k_nsize["accuracy_test"]))
                    np_deepstrom_kernel_k_std_accuracy_test.append(np.std(deepstrom_kernel_k_nsize["accuracy_test"]))
                ax.bar(np.arange(len(np_deepstrom_kernel_k_mean_accuracy_test))+bar_width + bar_width*2*i, np_deepstrom_kernel_k_mean_accuracy_test,
                       width=bar_width, label=str(d_translate_kernel[k_name]) + " $\phi_{nys}$", color=real_nys_color_by_kernel[k_name])
                ax.errorbar(np.arange(len(np_deepstrom_kernel_k_mean_accuracy_test))+bar_width + bar_width*2*i,
                            np_deepstrom_kernel_k_mean_accuracy_test, yerr=np_deepstrom_kernel_k_std_accuracy_test,
                            color="black", fmt='none', capsize=3)

        ax.set_ylim(min_acc, max_acc)
        ax.set_ylabel("Precision")
        ax.set_xlabel("Subsample Size")
        ax.legend(bbox_to_anchor=(-0.075, -0.6), loc="lower left", ncol=2)
        ax.set_xticks(np.arange(len(nys_size)) + bar_width * len(kernel_names))
        ax.set_xticklabels(sorted(nys_size))
        ax.tick_params(length=0)
        print(sorted(nys_size))
        ax.set_title("{} - Convolution level: {} conv blocks".format(DATANAME, d_translate_repr[r_qual]))
        # f.set_size_inches(7, 5)
        f.subplots_adjust(bottom=0.35)
        f.show()
        # f.savefig("/home/luc/PycharmProjects/deepFriedConvnets/main/experiments/graph_drawing/paper/cifar/multikernel/" +
        #           "acc_nsize_rqual_{}".format(r_qual))
        # exit()

