import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger
import matplotlib

matplotlib.rcParams.update({'font.size': 14})

pd.set_option('display.width', 1000)
dirnames = ["/home/luc/Resultats/Deepstrom/SVHN/june_2018/svhn_std_classif"]
filenames = ["gathered_results.csv"]
d_name = ["svhn"]
datanames = ["svhn"]

for h in range(len(dirnames)):
    DIRNAME = dirnames[h]
    FILENAME = filenames[h]
    DATANAME = datanames[h]
    min_acc = 0.00
    max_acc = 1.05
    # max_acc = 1.0
    linewidth = 0.9
    output_conv_dim = 512
    nb_classes = 10
    
    real_nys_marker = "s"
    
    learned_nys_marker = "x"
    
    linearity_color={
        "linear": "g",
        "non-linear": "m"
    }
    
    # kernel_marker = {
    #     "chi2_cpd": "x",
    #     "rbf": "d",
    #     "linear": "h",
    # }
    # kernel_color = {
    #     "chi2_cpd": "b",
    #     "linear": "r",
    #     "rbf": "c",
    # }
    
    dense_marker = "v"
    dense_color = "r"
    
    deepfried_marker = "8"
    deepfried_color = "b"
    
    d_translate_kernel = {
        "linear": "Linear",
        "chi2_cpd": "Chi2",
        "rbf": "Gaussian"
    }
    
    if __name__ == '__main__':
        filepath = os.path.join(DIRNAME, FILENAME)
        field_names = ["method_name",
                       "accuracy_val",
                       "accuracy_test",
                       "runtime",
                       "number_epoch",
                       "batch_size",
                       "repr_dim",
                       "two_layers_dense",
                       "kernel_deepstrom",
                       "gamma_kernel",
                       "constante_sigmoid",
                       "nb_layer_deepfried",
                       "subsample_size",
                       "validation_size",
                       "seed",
                       "non_linearity",
                       "real_nystrom",
                       "bad_repr",
                       "train_size"
                       ]
    
        df = pd.read_csv(filepath, names=field_names)
        df = df.apply(pd.to_numeric, errors="ignore")
        method_names = set(df["method_name"].values)
        kernel_names = set(df["kernel_deepstrom"].values)
        kernel_names.remove("None")
        # kernel_names.remove("laplacian")
        repr_dim = set(df["repr_dim"].values)
        repr_dim.remove("None")  # dtype: str
        # repr_dim.remove("16")
        nys_size = set(df["subsample_size"].values)
        # nys_size.remove("None")
        seed_values = set(df["seed"].values)
        batch_size = 128
        logger.debug("Nystrom possible sizes are: {}".format(nys_size))
        logger.debug("Kernel functions are: {}".format(kernel_names))
        logger.debug("Compared network types are: {}".format(method_names))
        logger.debug("Tested representation dimension are: {}".format(repr_dim))
    
        means_deepstrom = {}
    
        
    
        # plot deepstrom
        # ==============
        df_deepstrom = df[df["method_name"] == "deepstrom"]
        df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
        df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
        for i, k_name in enumerate(kernel_names):

            f, ax = plt.subplots()
            df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]
            non_lin_dfs = {
                "linear": df_deepstrom_kernel[df_deepstrom_kernel["non_linearity"] == "None"],
                # "non-linear": df_deepstrom_kernel[df_deepstrom_kernel["non_linearity"] != "None"]
            }
            # get the results of learned nystrom
            for linearity, df_deepstrom_kernel_lin_case in non_lin_dfs.items():
                df_deepstrom_kernel_w = df_deepstrom_kernel_lin_case[df_deepstrom_kernel_lin_case["real_nystrom"] == False]
                np_deepstrom_kernel_w_mean_accuracy_test = np.mean(np.array([
                    list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in seed_values
                ]), axis=0)
                np_deepstrom_kernel_w_std_accuracy_test = np.std(np.array([list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                                                           seed_values]), axis=0)

                np_param_nbr_deepstrom_kernel_w = (np.square(np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))))) +
                                                   # np.array(sorted(set(df_deepstrom_kernel_w["subsample_size"]))) * output_conv_dim +  # m x d
                                                   np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * 10)

                ax.errorbar(np_param_nbr_deepstrom_kernel_w,
                               np_deepstrom_kernel_w_mean_accuracy_test,
                               np_deepstrom_kernel_w_std_accuracy_test,
                               marker=learned_nys_marker, color=linearity_color[linearity], label="Adaptive-$\phi_{nys}$" + (" + relu" if linearity != "linear" else ""), linestyle="None", capsize=3)

                # get the results of vanilla nystrom
                df_deepstrom_kernel_k = df_deepstrom_kernel_lin_case[df_deepstrom_kernel_lin_case["real_nystrom"]]
                if len(df_deepstrom_kernel_k):
                    np_deepstrom_kernel_k_mean_accuracy_test = np.mean(
                        np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)
                    np_deepstrom_kernel_k_std_accuracy_test = np.std(
                        np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                  seed_values]), axis=0)

                    np_param_nbr_deepstrom_kernel_k = (np.square(np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"]))))) +
                                                       # np.array(sorted(set(df_deepstrom_kernel_k["subsample_size"]))) * output_conv_dim +  # m x d
                                                       np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"])))) * 10)

                    ax.errorbar(np_param_nbr_deepstrom_kernel_k,
                                   np_deepstrom_kernel_k_mean_accuracy_test,
                                   np_deepstrom_kernel_k_std_accuracy_test,
                                   marker=real_nys_marker, color=linearity_color[linearity], label="$\phi_{nys}$" + (" + relu" if linearity != "linear" else ""), linestyle="None", capsize=3)


            # plot dense
            # ==========
            df_dense = df[df["method_name"] == "dense"]
            df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
            df_dense = df_dense.sort_values(by=["repr_dim"])
            np_dense_mean_accuracy_test = np.mean(
                np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            np_dense_std_accuracy_test = np.std(
                np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            ax.errorbar(np.array(sorted([int(n) for n in repr_dim])) * output_conv_dim +
                           np.array(sorted([int(n) for n in repr_dim])) * nb_classes,
                           np_dense_mean_accuracy_test,
                           np_dense_std_accuracy_test,
                           color=dense_color,
                           marker=dense_marker,
                           label="$\phi_{nn}$", capsize=3, linestyle="None")

            # plot deepfried
            # ==============
            df_deepfried = df[df["method_name"] == "deepfriedconvnet"]
            np_deepfried_mean_accuracy_test = np.mean(
                np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            np_deepfried_std_accuracy_test = np.std(
                np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            print(np_deepfried_mean_accuracy_test, np_deepfried_std_accuracy_test)
            ax.errorbar(output_conv_dim * 3 + output_conv_dim * nb_classes,
                           np_deepfried_mean_accuracy_test,
                           np_deepfried_std_accuracy_test,
                           color=deepfried_color,
                           marker=deepfried_marker,
                           linestyle = "None",
                           label="Adaptive-$\phi_{ff}$", capsize=3)
            ax.set_ylim(min_acc, max_acc)
            ax.set_ylabel("Accuracy")
            
            ax.set_xlabel("symlog(# Learnable Parameters)")

            ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=4)
            ax.set_xscale("symlog")
            ax.set_title("{} Kernel - {}".format(d_translate_kernel[k_name], DATANAME), y=1.2)

            ax_twin = ax.twiny()
            ax_twin.set_xscale("symlog")
            ax_twin.set_xlim(ax.get_xlim())
            ax_twin.set_xticks(np_param_nbr_deepstrom_kernel_w)

            # if i == 0:
            ax_twin.set_xlabel("Subsample Size")
            ax_twin.set_xticklabels(sorted(set(df_deepstrom_kernel_w["subsample_size"])))

            f.set_size_inches(8, 6)
            f.tight_layout()
            f.subplots_adjust(bottom=0.3)
            # f.show()
            out_dir_path = "/home/luc/PycharmProjects/deepFriedConvnets/" \
                           "main/experiments/graph_drawing/paper/{}/sota/learnable_parameters".format(d_name[h])
            out_name = "acc_param_{}".format(k_name)
            out_path = os.path.join(out_dir_path, out_name)
            f.savefig(out_path)


    # l_dense = []
    # l_nys = []
    # print(DATANAME)
    # print("dense")
    # for i, r_dim in enumerate(sorted([int(n) for n in repr_dim])):
    #     df_dense = df[df["method_name"] == "dense"]
    #     df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
    #     df_dense = df_dense.sort_values(by=["repr_dim"])
    #     np_dense_mean_accuracy_test = np.mean(
    #         np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
    #                   seed_values]), axis=0)
    #
    #     print(r_dim, np_dense_mean_accuracy_test[i])
    #     l_dense.append(np_dense_mean_accuracy_test[i])
    # print()
    # print("deepfried")
    # print(np_deepfried_mean_accuracy_test[0])
    # print()
    # print("deepstrom")
    # df_deepstrom = df[df["method_name"] == "deepstrom"]
    # df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
    # df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
    # for i, r_dim in enumerate(sorted([int(n) for n in nys_size])):
    #     df_deepstrom_size = df_deepstrom_sort[df_deepstrom_sort["subsample_size"] == r_dim]
    #     np_deepstrom_mean = np.mean(np.array([
    #         list(df_deepstrom_size[df_deepstrom_size["seed"] == seed_v]["accuracy_test"]) for seed_v in
    #         seed_values
    #     ]), axis=0)
    #     print(r_dim, max(np_deepstrom_mean))
    #     l_nys.append(max(np_deepstrom_mean))
    #
    # print("dense2")
    # print(" ".join(["%.2f" % (x * 100) for x in l_dense]))
    # print("nys2")
    # print(" ".join(["%.2f" % (x * 100) for x in l_nys]))
# f.set_size_inches(11, 15.5)
# f.savefig("acc_param")

# plt.xticks(sorted([int(n) for n in nys_size]))
