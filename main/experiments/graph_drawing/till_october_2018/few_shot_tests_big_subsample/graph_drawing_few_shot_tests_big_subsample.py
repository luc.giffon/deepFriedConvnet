
# coding: utf-8

# In[74]:


import os
import pathlib

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from skluc.main.utils import logger


pd.set_option('display.width', 1000)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('display.max_columns', None)

flatten = lambda l: [item for sublist in l for item in sublist]


# In[2]:


FILENAME = "gathered_results.csv"
DIRNAME = "/home/luc/Resultats/Deepstrom/september_2018/few_shot_tests_big_subsample"
filepath = os.path.join(DIRNAME, FILENAME)


# In[119]:


min_acc = 0.6
max_acc = 1.0
linewidth = 0.9

real_nys_marker = "s"

learned_nys_marker = "x"

dense_marker = "v"
dense_color = "r"

deepfried_marker = "8"
deepfried_color = "b"

d_translate_kernel = {
    "linear": "Linear",
    "chi2_cpd": "Chi2",
    "rbf": "Gaussian",
}


# In[19]:


df_root = pd.read_csv(filepath)
df_root


# In[18]:


df = df_root.loc[:, ["accuracy", "time", "--non-linear", "--num-class-ep-test", "--num-class-ep-train", "--num-epoch", "--num-query-ex", "--num-supp-ex", "--nys-size", "--out-dim", "kernel", "network"]]
df.head()


# In[38]:


df.sort_values(by="accuracy", ascending=False).head()


# In[30]:


kernel_names = set(df["kernel"].values)
logger.debug("Kernel functions are: {}".format(kernel_names))

non_linearities = set(df["--non-linear"])
logger.debug("non_linearities: {}".format(non_linearities))

nystrom_sizes = set(df["--nys-size"])
logger.debug(f"Nystrom sizes: {nystrom_sizes}")

output_dims = set(df["--out-dim"])
logger.debug(f"Output dimension: {output_dims}")


# In[125]:


step_between_groups = 0.25
group_size = 2
step_inside_groups = 0.15
bar_width = 0.2


for idx_plot, n_size in enumerate(sorted(nystrom_sizes)):
    fig, axs = plt.subplots()
    df_n_size = df[df["--nys-size"] == n_size]
    group_of_bins = []
    group_of_bins_labels = []
    for o_dim in sorted(output_dims):
        df_o_dim = df_n_size[df_n_size["--out-dim"] == o_dim]
        
        current_group_of_bins = []
        current_group_of_bins_labels = []
        for k_name in sorted(kernel_names):
            df_k_name = df_o_dim[df_o_dim["kernel"] == k_name]
            for n_lin in sorted(non_linearities):
                try:
                    df_n_lin = df_k_name[df_k_name["--non-linear"] == n_lin]
                    current_group_of_bins.append(df_n_lin["accuracy"].values[0])
                    current_group_of_bins_labels.append(f"{k_name}-{'non-lin' if n_lin else 'lin'}-{o_dim}")
                except:
                    pass
        
        group_of_bins.append(current_group_of_bins)
        group_of_bins_labels.append(current_group_of_bins_labels)
    
    x_locations_total = []
    x_labels_total = []
    for i, group in enumerate(group_of_bins):
        x_locations = np.linspace(start=i*(group_size) + step_between_groups, stop=(i+1)*(group_size) - step_between_groups, num=len(group))
        x_locations_total.extend(list(x_locations))
        x_labels_total.extend(list(group_of_bins_labels[i]))
        axs.bar(x_locations, 
                group,
                width=bar_width,
                align="edge")
    axs.set_ylim(min_acc, max_acc)        
    axs.set_ylabel('Accuracies')
    title = f'Deepstrom - cifar100 - size {n_size} - few shot - big subsample'
    axs.set_title(f'Deepstrom - cifar100 - size {n_size} - few shot - big subsample')
    base_out_dir = os.path.join(os.path.abspath(__file__.split(".")[0]), "images")
    out_name = title.replace(" - ", "_")

    pathlib.Path(base_out_dir).mkdir(parents=True, exist_ok=True)
    out_path = os.path.join(base_out_dir, out_name)
    plt.xticks(x_locations_total, x_labels_total, rotation=75)

    # plt.set_size_inches(8, 6)
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.35)
    plt.savefig(out_path)

    plt.show()


