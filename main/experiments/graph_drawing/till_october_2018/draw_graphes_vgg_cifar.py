import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

pd.set_option('display.width', 1000)

DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/big_grid_vgg_cifar10"
FILENAME = "gathered_results.csv"

GAMMA_DEEPFRIED = 0.3727593720314938
GAMMA_EXP_CHI2 = 0.3727593720314938
GAMMA_RBF = 0.13894954943731375
min_acc = 0.05
max_acc = 1.0
linewidth = 0.9
output_conv_dim = 512
kernel_marker = {
    "chi2_cpd": "x",
    "linear": "o",
    "chi2_pd": "v",
    "rbf": "d",
    "chi2_exp_cpd": "h"
}
kernel_color = {
    "chi2_cpd": "b",
    "linear": "g",
    "chi2_pd": "r",
    "rbf": "c",
    "chi2_exp_cpd": "m"
}

dense_marker = {
    "True": "+",
    "False": "*"
}

dense_color = {
    "True": "k",
    "False": "y"
}

deepfried_marker = {
    "1": "8",
    "2": "s"
}

deepfried_color = {
    "1": "#F289E7",
    "2": "#A2EF3E"
}

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    # get only the experiments that has been run
    df = df[df["accuracy"] != str(None)]
    # get only the results of the specified gammas
    truth = (
                    ((df["method_name"] == "deepfriedconvnet") & (df["gamma_kernel"] == str(GAMMA_DEEPFRIED))) |
                    ((df["method_name"] == "deepstrom") & (df["kernel_deepstrom"] == "rbf") & (df["gamma_kernel"] == str(GAMMA_RBF))) |
                    ((df["method_name"] == "deepstrom") & (df["kernel_deepstrom"] == "chi2_exp_cpd") & (df["gamma_kernel"] == str(GAMMA_EXP_CHI2))) |
                    (df["gamma_kernel"] == str(None))
    )
    df = df.apply(pd.to_numeric, errors="ignore")
    df = df[truth]
    df = df[(df["kernel_deepstrom"]) != "sigmoid"]
    # df[df["repr_dim"] != str(None)]["repr_dim"] = df[df["repr_dim"] != str(None)]["repr_dim"].astype(np.int)
    # df[df["subsample_size"] != str(None)]["subsample_size"] = df[df["subsample_size"] != str(None)]["subsample_size"].astype(np.int)
    batche_sizes = set(df["batch_size"].values)
    nys_sizes = set(df["subsample_size"].values)
    nys_sizes.remove("None")
    # nys_sizes = [int(r) for r in nys_sizes]
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    repr_dim = set(df["repr_dim"].values)
    repr_dim.remove("None")
    # repr_dim = [int(r) for r in repr_dim]
    print(kernel_names)
    print(method_names)
    print(nys_sizes)
    print(repr_dim)
    for b_size in batche_sizes:
        # acc/nb subsample
        df_batch = df[df["batch_size"] == b_size]
        for r_dim in repr_dim.union(nys_sizes):
            f, ax = plt.subplots()
            df_batch_repr = df_batch[(df_batch["repr_dim"] == r_dim) | (df_batch["repr_dim"] == str(None))]
            df_deepstrom = df_batch_repr[df_batch_repr["method_name"] == "deepstrom"]
            df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
            df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
            for k_name in kernel_names:
                df_deepstrom_kernel = df_deepstrom[df_deepstrom["kernel_deepstrom"] == k_name]
                df_deepstrom_kernel_w = df_deepstrom_kernel[df_deepstrom_kernel["repr_dim"] != str(None)]
                df_deepstrom_kernel_k = df_deepstrom_kernel[(df_deepstrom_kernel["repr_dim"] == str(None)) &
                                                            (df_deepstrom_kernel["subsample_size"] == int(r_dim))]
                if len(df_deepstrom_kernel_k):
                    plt.scatter(df_deepstrom_kernel_k["subsample_size"], df_deepstrom_kernel_k["accuracy"],
                                marker="s", color=kernel_color[k_name], label=k_name + " $k^{-1/2}$")
                    # circle_k = plt.Circle((df_deepstrom_kernel_k["subsample_size"], df_deepstrom_kernel_k["accuracy"]),
                    #                       1, color=kernel_color[k_name], label="salut", fill=False)
                    # ax.add_artist(circle_k)
                    # ax.arrow(0, 0, 0.5, 0.5, head_width=0.05, head_length=0.1, fc='k', ec='k')
                plt.scatter(list(df_deepstrom_kernel_w["subsample_size"]), list(df_deepstrom_kernel_w["accuracy"]),
                            marker=kernel_marker[k_name], color=kernel_color[k_name], label=k_name)

            df_dense = df_batch_repr[df_batch_repr["method_name"] == "dense"]
            if len(df_dense):
                acc_val_dense_1 = float(df_dense[df_dense["two_layers_dense"] == False]["accuracy"])
                plt.plot(sorted([int(n) for n in nys_sizes]), [acc_val_dense_1 for _ in nys_sizes], color=dense_color["False"], linewidth=linewidth, label="1 layer")

            df_deefired = df_batch_repr[df_batch_repr["method_name"] == "deepfriedconvnet"]
            acc_val_deepfried_1 = float(df_deefired[df_deefired["nb_layer_deepfried"] == "1"]["accuracy"])
            plt.plot(sorted([int(n) for n in nys_sizes]), [acc_val_deepfried_1 for _ in nys_sizes],
                     color=deepfried_color["1"], linewidth=linewidth, label="1 stack")
            plt.title("Accuracy by number of subsample for \n batch size = {} and representation dim = {}".format(b_size, r_dim))
            plt.ylabel("Accuracy")
            plt.xlabel("symlog(Subsample size)")
            plt.xscale("symlog")
            plt.legend()
            plt.ylim(min_acc, max_acc)
            plt.xticks(sorted([int(n) for n in nys_sizes]))
            plt.show()

        # acc/nbparam
        # acc/trainingtime
        df_deepstrom = df_batch[df_batch["method_name"] == "deepstrom"]
        for k_name in kernel_names:
            fig1, ax1 = plt.subplots()
            df_deepstrom_kernel = df_deepstrom[df_deepstrom["kernel_deepstrom"] == k_name]
            accuracies = []
            nb_param = []
            for r_dim in repr_dim:
                df_deepstrom_rdim = df_deepstrom_kernel[df_deepstrom_kernel["repr_dim"] == r_dim]
                df_deepstrom_rdim["repr_dim"] = df_deepstrom_rdim["repr_dim"].astype(np.int)
                df_deepstrom_rdim["subsample_size"] = df_deepstrom_rdim["subsample_size"].astype(np.int)
                for n_size in nys_sizes:
                    df_deepstrom_rdim_nys_size = df_deepstrom_rdim[df_deepstrom_rdim["subsample_size"] == int(n_size)]
                    if len(df_deepstrom_rdim_nys_size):
                        nb_param.append(int(r_dim) * int(n_size))
                        accuracies.append(float(df_deepstrom_rdim_nys_size["accuracy"]))

            print("Kernel: {}; len(nb_param): {}; len(accuracies): {}".format(k_name, len(nb_param), len(accuracies)))
            ax1.scatter(nb_param, accuracies,
                        marker=kernel_marker[k_name], color=kernel_color[k_name], label=k_name)

            df_dense = df_batch[df_batch["method_name"] == "dense"]

            df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
            df_dense_1 = df_dense[df_dense["two_layers_dense"] == False]
            accuracies_1 = []
            nb_param_1 = []
            for r_dim in repr_dim:
                df_dense_1_repr_dim = df_dense_1[df_dense_1["repr_dim"] == int(r_dim)]
                if len(df_dense_1_repr_dim):
                    accuracies_1.append(float(df_dense_1_repr_dim["accuracy"]))
                    nb_param_1.append(int(r_dim) * output_conv_dim)
            ax1.scatter(nb_param_1, accuracies_1, color=dense_color["False"], marker=dense_marker["False"],
                linewidth=linewidth, label="1 layer")

            df_deefired = df_batch[df_batch["method_name"] == "deepfriedconvnet"]

            acc_val_deepfried_1 = float(df_deefired[df_deefired["nb_layer_deepfried"] == "1"]["accuracy"])
            ax1.scatter(output_conv_dim * 3, acc_val_deepfried_1,
                     color=deepfried_color["1"], linewidth=linewidth+1, label="1 stack", marker=deepfried_marker["1"], )

            ax1.set_title("Accuracy by number of parameters for batch size = {}".format(b_size))
            ax1.set_ylabel("Accuracy")
            ax1.set_xlabel("symlog(Nb_param)")
            ax1.set_xscale("symlog")
            ax1.legend()
            ax1.set_ylim(min_acc, max_acc)
            plt.show()
            a=1
        # print(df_deepstrom)