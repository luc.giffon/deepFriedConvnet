import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

from skluc.main.utils import logger

pd.set_option('display.width', 1000)

DIRNAME = "/home/luc/Resultats/Deepstrom/CIFAR10/vgg19/april_2018/big_grid_vgg_cifar10_3_little_subsample"
FILENAME = "gathered_results.csv"

GAMMA_DEEPFRIED = 0.3727593720314938
GAMMA_EXP_CHI2 = 0.3727593720314938
GAMMA_RBF = 0.13894954943731375
min_acc = 0.68
max_acc = 0.93
linewidth = 0.9
output_conv_dim = 512
kernel_marker = {
    "chi2_cpd": "x",
    "rbf": "d",
    "summed": "h",
    "laplacian": "v"
}
kernel_color = {
    "chi2_cpd": "b",
    "laplacian": "r",
    "rbf": "c",
    "summed": "m"
}

dense_marker = {
    16: "+",
    128: "o",
    1024: "H"
}

dense_color = {
    16: "k",
    128: "g",
    1024: "#A2EF3E"
}

deepfried_marker = "8"

deepfried_color = "#F289E7"

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    df = df.apply(pd.to_numeric, errors="ignore")
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    # kernel_names.remove("laplacian")
    repr_dim = set(df["repr_dim"].values)
    repr_dim.remove("None")  # dtype: str
    # repr_dim.remove("16")
    nys_size = set(df["subsample_size"].values)
    nys_size.remove("None")
    seed_values = set(df["seed"].values)
    batch_size = 128
    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))

    means_deepstrom = {}

    # plot deepstrom
    # ==============
    df_deepstrom = df[df["method_name"] == "deepstrom"]
    df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
    df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
    for k_name in kernel_names:
        df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]
        # get the results of learned nystrom
        df_deepstrom_kernel_w = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"] == False]
        np_deepstrom_kernel_w_mean_accuracy_test = np.mean(np.array([
            list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in seed_values
        ]), axis=0)
        np_deepstrom_kernel_w_std_accuracy_test = np.std(np.array([list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                                                   seed_values]), axis=0)
        # get the results of vanilla nystrom
        df_deepstrom_kernel_k = df_deepstrom_kernel[df_deepstrom_kernel["real_nystrom"]]
        if len(df_deepstrom_kernel_k):
            np_deepstrom_kernel_k_mean_accuracy_test = np.mean(
                np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            np_deepstrom_kernel_k_std_accuracy_test = np.std(
                np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)

            plt.errorbar(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))),
                        np_deepstrom_kernel_k_mean_accuracy_test,
                        np_deepstrom_kernel_k_std_accuracy_test,
                        marker="s", color=kernel_color[k_name], label=k_name + " $k^{-1/2}$", linestyle="None", capsize=3)

        plt.errorbar(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))),
                    np_deepstrom_kernel_w_mean_accuracy_test,
                    np_deepstrom_kernel_w_std_accuracy_test,
                    marker=kernel_marker[k_name], color=kernel_color[k_name], label=k_name, linestyle="None", capsize=3)
        # plt.legend()
        # plt.show()
        # exit()
        # plot dense
        # ==========
        df_dense = df[df["method_name"] == "dense"]
        for r_dim in repr_dim:
            df_dense_r_dim = df_dense[df_dense["repr_dim"] == r_dim]
            np_dense_mean_accuracy_test = np.mean(
                np.array([list(df_dense_r_dim[df_dense_r_dim["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            np_dense_std_accuracy_test = np.std(
                np.array([list(df_dense_r_dim[df_dense_r_dim["seed"] == seed_v]["accuracy_test"]) for seed_v in
                          seed_values]), axis=0)
            plt.errorbar(sorted([int(n) for n in nys_size]),
                        [np_dense_mean_accuracy_test[0] for _ in nys_size],
                        # [np_dense_std_accuracy_test[0] for _ in nys_size],
                        color=dense_color[int(r_dim)],
                        # marker=dense_marker[int(r_dim)],
                        label="Dense {}".format(r_dim), capsize=3)

        # plot deepfried
        # ==============
        df_deepfried = df[df["method_name"] == "deepfriedconvnet"]
        np_deepfried_mean_accuracy_test = np.mean(
            np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        np_deepfried_std_accuracy_test = np.std(
            np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                      seed_values]), axis=0)
        plt.errorbar(sorted([int(n) for n in nys_size]),
                    [np_deepfried_mean_accuracy_test[0] for _ in nys_size],
                    # [np_deepfried_std_accuracy_test[0] for _ in nys_size],
                    color=deepfried_color,
                    # marker=deepfried_marker,
                    label="Deepfried Convnet", capsize=3)
        plt.title("Accuracy by number of subsample for \n batch size = {}".format(batch_size))
        plt.ylabel("Accuracy")
        plt.xlabel("symlog(Subsample size)")
        plt.xscale("symlog")
        # plt.ylim(min_acc, max_acc)
        plt.xticks(sorted([int(n) for n in nys_size]))
        plt.legend()
        plt.show()