import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import pathlib

from skluc.main.utils import logger
import matplotlib

matplotlib.rcParams.update({'font.size': 14})

pd.set_option('display.width', 1000)
dirname = "/home/luc/Resultats/Deepstrom/MNIST/mnist_pretrain_expe_1"
filename = "gathered_results.csv"
d_name = "mnist"
# FILENAME = "gathered_results.csv"
dataname = "MNIST"

DIRNAME = dirname
FILENAME = filename
DATANAME = dataname
min_acc = 0.00
max_acc = 1.05
# max_acc = 1.0
linewidth = 0.9
output_conv_dim = 512
nb_classes = 10
batch_size = 128

real_nys_marker = "s"

learned_nys_marker = "o"

linearity_color = "g"

dense_marker = "v"
dense_color = "r"

deepfried_marker = "X"
deepfried_color = "b"

d_translate_kernel = {
    "linear": "Linear",
    "chi2_cpd": "Chi2",
    "rbf": "Gaussian",
    "chi2_exp_cpd": "Exp Chi2"
}
d_kernel_color = {
    "linear": "m",
    "chi2_cpd": "g",
    "rbf": "c"
}

if __name__ == '__main__':
    filepath = os.path.join(DIRNAME, FILENAME)
    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "two_layers_dense",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    df = df.apply(pd.to_numeric, errors="ignore")
    df = df[((df["subsample_size"] != "16") & (df["subsample_size"] != "64")) | (df["subsample_size"] == "None")]
    method_names = set(df["method_name"].values)
    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    # kernel_names.remove("laplacian")
    repr_dim = set(df["repr_dim"].values)
    repr_dim.remove("None")  # dtype: str
    # repr_dim.remove("16")
    nys_size = set(df["subsample_size"].values)
    nys_size.remove("None")
    seed_values = set(df["seed"].values)
    logger.debug("Nystrom possible sizes are: {}".format(nys_size))
    logger.debug("Kernel functions are: {}".format(kernel_names))
    logger.debug("Compared network types are: {}".format(method_names))
    logger.debug("Tested representation dimension are: {}".format(repr_dim))

    means_deepstrom = {}


    # plot deepstrom
    # ==============
    df_deepstrom = df[df["method_name"] == "deepstrom"]
    df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
    df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
    f, ax = plt.subplots()
    for i, k_name in enumerate(kernel_names):

        df_deepstrom_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]
        non_lin_dfs = {
            "linear": df_deepstrom_kernel[df_deepstrom_kernel["non_linearity"] == "None"],
        }
        # get the results of learned nystrom
        for linearity, df_deepstrom_kernel_lin_case in non_lin_dfs.items():
            df_deepstrom_kernel_w = df_deepstrom_kernel_lin_case[df_deepstrom_kernel_lin_case["real_nystrom"] == False]
            np_deepstrom_kernel_w_mean_accuracy_test = np.mean(np.array([
                list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in seed_values
            ]), axis=0)
            np_deepstrom_kernel_w_std_accuracy_test = np.std(np.array([list(df_deepstrom_kernel_w[df_deepstrom_kernel_w["seed"] == seed_v]["accuracy_test"]) for seed_v in
                                                                       seed_values]), axis=0)

            np_param_nbr_deepstrom_kernel_w = (np.square(np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"]))))) +
                                               # np.array(sorted(set(df_deepstrom_kernel_w["subsample_size"]))) * output_conv_dim +  # m x d
                                               np.array(sorted(list(set(df_deepstrom_kernel_w["subsample_size"])))) * 10)

            ax.errorbar(np_param_nbr_deepstrom_kernel_w,
                           np_deepstrom_kernel_w_mean_accuracy_test,
                           np_deepstrom_kernel_w_std_accuracy_test,
                           marker=learned_nys_marker, markersize="10",
                        linestyle="None", color=d_kernel_color[k_name], label="Adaptive Deepström" + (" + relu" if linearity != "linear" else ""),  capsize=3)

            # get the results of vanilla nystrom
            df_deepstrom_kernel_k = df_deepstrom_kernel_lin_case[df_deepstrom_kernel_lin_case["real_nystrom"]]
            if len(df_deepstrom_kernel_k):
                np_deepstrom_kernel_k_mean_accuracy_test = np.mean(
                    np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                              seed_values]), axis=0)
                np_deepstrom_kernel_k_std_accuracy_test = np.std(
                    np.array([list(df_deepstrom_kernel_k[df_deepstrom_kernel_k["seed"] == seed_v]["accuracy_test"]) for seed_v in
                              seed_values]), axis=0)

                np_param_nbr_deepstrom_kernel_k = (#np.square(np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"]))))) +
                                                   #np.array(sorted(set(df_deepstrom_kernel_k["subsample_size"]))) * output_conv_dim +  # m x d
                                                   np.array(sorted(list(set(df_deepstrom_kernel_k["subsample_size"])))) * 10)

                ax.errorbar(np_param_nbr_deepstrom_kernel_k,
                               np_deepstrom_kernel_k_mean_accuracy_test,
                               np_deepstrom_kernel_k_std_accuracy_test, linestyle="None", markersize="10",
                               marker=real_nys_marker, color=d_kernel_color[k_name], label="Deepström" + (" + relu" if linearity != "linear" else ""),  capsize=3)


    # plot dense
    # ==========
    df_dense = df[df["method_name"] == "dense"]
    df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
    df_dense = df_dense.sort_values(by=["repr_dim"])
    np_dense_mean_accuracy_test = np.mean(
        np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                  seed_values]), axis=0)
    np_dense_std_accuracy_test = np.std(
        np.array([list(df_dense[df_dense["seed"] == seed_v]["accuracy_test"]) for seed_v in
                  seed_values]), axis=0)
    ax.errorbar(np.array(sorted([int(n) for n in repr_dim])) * output_conv_dim +
                   np.array(sorted([int(n) for n in repr_dim])) * nb_classes,
                   np_dense_mean_accuracy_test,
                   np_dense_std_accuracy_test,
                   color=dense_color,
                   marker=dense_marker,
                linestyle="None", markersize="10",
                   label="Fully Connected", capsize=3)

    # plot deepfried
    # ==============
    df_deepfried = df[df["method_name"] == "deepfriedconvnet"]
    np_deepfried_mean_accuracy_test = np.mean(
        np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                  seed_values]), axis=0)
    np_deepfried_std_accuracy_test = np.std(
        np.array([list(df_deepfried[df_deepfried["seed"] == seed_v]["accuracy_test"]) for seed_v in
                  seed_values]), axis=0)
    ax.errorbar(output_conv_dim * 3 + output_conv_dim * nb_classes,
                   np_deepfried_mean_accuracy_test,
                   np_deepfried_std_accuracy_test,
                   color=deepfried_color,
                   marker=deepfried_marker,
                    linestyle="None", markersize="10",
                   label="Adaptative DeepfriedConvnet", capsize=3)
    ax.set_ylim(min_acc, max_acc)
    ax.set_ylabel("Accuracy")

    ax.set_xlabel("# Parameters")

    # ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=2)
    ax.set_xscale("symlog")
    # ax.set_title("{} Kernel - {}".format(d_translate_kernel[k_name], DATANAME), y=1.2)

    # ax_twin = ax.twiny()
    # ax_twin.set_xscale("symlog")
    # ax_twin.set_xlim(ax.get_xlim())
    # ax_twin.set_xticks(np_param_nbr_deepstrom_kernel_w)
    #
    # # if i == 0:
    # ax_twin.set_xlabel("Subsample Size")
    # ax_twin.set_xticklabels(sorted(set(df_deepstrom_kernel_w["subsample_size"])))

    f.set_size_inches(8, 6)
    f.tight_layout()
    # f.subplots_adjust(bottom=0.3)

    base_out_dir = os.path.join(os.path.abspath(__file__.split(".")[0]), "images")
    pathlib.Path(base_out_dir).mkdir(parents=True, exist_ok=True)
    out_name = "acc_param"
    out_path = os.path.join(base_out_dir, out_name)
    f.savefig(out_path)
