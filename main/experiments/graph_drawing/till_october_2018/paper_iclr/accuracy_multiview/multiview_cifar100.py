import os
import pathlib

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

pd.set_option('display.expand_frame_repr', False)

from skluc.main.utils import logger

matplotlib.rcParams.update({'font.size': 14})

pd.set_option('display.width', 1000)

if __name__ == "__main__":
    # Files parameters
    ###################
    FILENAME = "gathered_results.csv"
    DIRNAME = "/home/luc/Resultats/Deepstrom/september_2018/multiview_cifar100/re_gatherered_multiview_cifar100/multiview_cifar100_plus_rbf/multiview_cifar100_plus_poly2"
    filepath = os.path.join(DIRNAME, FILENAME)

    # figure parameters
    ###################
    min_acc = 0.6
    max_acc = 0.8
    # max_acc = 1.0
    linewidth = 0.9
    nb_classes = 10

    real_nys_marker = "s"

    learned_nys_marker = "x"

    linearity_color = "g"

    dense_marker = "v"
    dense_color = "r"

    deepfried_marker = "8"
    deepfried_color = "b"

    d_translate_kernel = {
        "linear": "Linear",
        "chi2_cpd": "Chi2",
        "rbf": "Gaussian",
        "polynomial2": "Polynomial2"
    }
    ######################
    # df initialization
    ######################

    batch_size = 64  # always the same batch size

    field_names = ["method_name",
                   "accuracy_val",
                   "accuracy_test",
                   "runtime_train",
                   "number_epoch",
                   "batch_size",
                   "repr_dim",
                   "kernel_deepstrom",
                   "gamma_kernel",
                   "constante_sigmoid",
                   "nb_layer_deepfried",
                   "subsample_size",
                   "validation_size",
                   "seed",
                   "non_linearity",
                   "real_nystrom",
                   "repr_quality",
                   "train_size",
                   "dropout",
                   "dataset",
                   "wafter",
                   "multiview"
                   ]

    df = pd.read_csv(filepath, names=field_names)
    df = df[df["accuracy_test"] != "None"]
    df = df.apply(pd.to_numeric, errors="ignore")

    df = df[df["dataset"] == "cifar100"]
    df = df[((df["method_name"] == "deepstrom") & (df["multiview"] != "None")) | (df["method_name"] != "deepstrom")]
    df = df[df["repr_quality"] != "block4_conv4"]

    df.loc[df['non_linearity'].str.contains('relu'), 'non_linearity'] = 'relu'
    df.loc[df['non_linearity'].str.contains('tan'), 'non_linearity'] = 'tan'

    df = df[(df["non_linearity"] == "None") | (df["non_linearity"] == "tan")]

    df = df.apply(pd.to_numeric, errors="ignore")
    df.real_nystrom = df.real_nystrom.astype(bool)
    # print(df.iloc[0])
    method_names = set(df["method_name"].values)
    logger.debug("Compared network types are: {}".format(method_names))

    datasets = set(df["dataset"].values)
    logger.debug("datasets: {}".format(datasets))

    kernel_names = set(df["kernel_deepstrom"].values)
    kernel_names.remove("None")
    # kernel_names.remove("chi2_exp_cpd")
    logger.debug("Kernel functions are: {}".format(kernel_names))

    seed_values = set(df["seed"].values)
    logger.debug("seed values: {}".format(seed_values))

    train_sizes = set(df["train_size"])
    logger.debug("train sizes: {}".format(train_sizes))

    dropout_values = set(df["dropout"].values)

    cut_layers = set(df["repr_quality"])
    logger.debug("cut layers: {}".format(cut_layers))
    cut_layers_output_conv_dim = {
        "block5_pool": 512,
        "block5_conv4": 2048,
        "block3_pool": 4096
    }

    cut_layers_nb_filters = {
        "block5_pool": 512,
        "block5_conv4": 512,
        "block3_pool": 256
    }

    non_linearities = set(df["non_linearity"])
    logger.debug("non_linearities: {}".format(non_linearities))

    multiview_methods = set(df["multiview"])
    multiview_methods.remove("None")
    logger.debug("multiview methods: {}".format(multiview_methods))

    w_after = set(df["wafter"])
    w_after.remove("None")
    logger.debug("w after: {}".format(w_after))

    # exit()
    ###########################

    # df processing
    ##############

    for h, DATANAME in enumerate(datasets):
        logger.debug(DATANAME)
        df_dataname = df[df["dataset"] == DATANAME]
        for c, CUT_LAYER in enumerate(cut_layers):
            if CUT_LAYER != "block3_pool":
                continue
            logger.debug(CUT_LAYER)
            df_cutlayer = df_dataname[df_dataname["repr_quality"] == CUT_LAYER]
            output_conv_dim = cut_layers_output_conv_dim[CUT_LAYER]
            nb_filter = cut_layers_nb_filters[CUT_LAYER]
            means_deepstrom = {}

            # plot deepstrom
            # ==============
            df_deepstrom = df_cutlayer[df_cutlayer["method_name"] == "deepstrom"]
            df_deepstrom["subsample_size"] = df_deepstrom["subsample_size"].astype(np.int)
            df_deepstrom_sort = df_deepstrom.sort_values(by=["subsample_size"])
            for i, k_name in enumerate(sorted(kernel_names)):
                df_deepstrom_sort_kernel = df_deepstrom_sort[df_deepstrom_sort["kernel_deepstrom"] == k_name]

                for mulmet_i, multiview_method in enumerate(multiview_methods):
                    if multiview_method != "concat":
                        continue
                    df_deepstrom_mulmet = df_deepstrom_sort_kernel[df_deepstrom_sort_kernel["multiview"] == multiview_method]

                    for wafter_i, wafter in enumerate(w_after):
                        df_deepstrom_wafter = df_deepstrom_mulmet[df_deepstrom_mulmet["wafter"] == wafter]
                        f, ax = plt.subplots()

                        # get the results of learned nystrom

                        nys_sizes = sorted(set(df_deepstrom_wafter["subsample_size"].values))
                        logger.debug("Nystrom possible sizes are: {}".format(nys_sizes))

                        df_deepstrom_kernel_w = df_deepstrom_wafter[df_deepstrom_wafter["real_nystrom"] == False]
                        df_deepstrom_kernel_w = df_deepstrom_kernel_w.apply(pd.to_numeric, errors="ignore")

                        np_deepstrom_kernel_w_mean_accuracy_test = []
                        np_deepstrom_kernel_w_std_accuracy_test = []
                        for n_size in nys_sizes:
                            np_deepstrom_kernel_w_mean_accuracy_test.append(np.mean(df_deepstrom_kernel_w[df_deepstrom_kernel_w["subsample_size"] == n_size]["accuracy_test"]))
                            np_deepstrom_kernel_w_std_accuracy_test.append(np.std(df_deepstrom_kernel_w[df_deepstrom_kernel_w["subsample_size"] == n_size]["accuracy_test"]))

                        # m x c
                        if multiview_method == "concat":
                            nb_param_after_phi_deepstrom_kernel = np.array(sorted(nys_sizes)) * nb_filter * nb_classes
                            if wafter is True:
                                nb_param_deepstrom = np.zeros(len(nys_sizes))
                            else:
                                nb_param_deepstrom = np.square(np.array(sorted(nys_sizes))) * nb_filter
                        else:
                            nb_param_after_phi_deepstrom_kernel = np.array(sorted(nys_sizes)) * nb_classes
                            if wafter is True:
                                nb_param_deepstrom = np.square(np.array(sorted(nys_sizes)))
                            else:
                                nb_param_deepstrom = np.square(np.array(sorted(nys_sizes))) * nb_filter


                        np_param_nbr_deepstrom_kernel_w = (
                                nb_param_deepstrom +  # m x m -> W matrix
                                np.array(sorted(nys_sizes)) * output_conv_dim +  # m x d -> subsample storage
                                nb_param_after_phi_deepstrom_kernel
                        )

                        ax.errorbar(np_param_nbr_deepstrom_kernel_w,
                                    np_deepstrom_kernel_w_mean_accuracy_test,
                                    np_deepstrom_kernel_w_std_accuracy_test,
                                    marker=learned_nys_marker, color=linearity_color,
                                    label="Adaptative Deepström",
                                     capsize=3)

                        # get the results of vanilla nystrom
                        df_deepstrom_kernel_k = df_deepstrom_wafter[df_deepstrom_wafter["real_nystrom"]]
                        df_deepstrom_kernel_k = df_deepstrom_kernel_k.apply(pd.to_numeric, errors="ignore")

                        if len(df_deepstrom_kernel_k):
                            np_deepstrom_kernel_k_mean_accuracy_test = []
                            np_deepstrom_kernel_k_std_accuracy_test = []
                            for n_size in nys_sizes:
                                np_deepstrom_kernel_k_mean_accuracy_test.append(np.mean(
                                    df_deepstrom_kernel_k[df_deepstrom_kernel_k["subsample_size"] == n_size][
                                        "accuracy_test"]))
                                np_deepstrom_kernel_k_std_accuracy_test.append(np.std(
                                    df_deepstrom_kernel_k[df_deepstrom_kernel_k["subsample_size"] == n_size][
                                        "accuracy_test"]))

                            if multiview_method == "concat":
                                nb_param_after_phi_deepstrom_kernel = np.array(
                                    sorted(nys_sizes)) * nb_filter * nb_classes
                                if wafter is True:
                                    nb_param_deepstrom = np.zeros(len(nys_sizes))
                                else:
                                    nb_param_deepstrom = np.array(sorted(nys_sizes)) * nb_filter
                            else:
                                nb_param_after_phi_deepstrom_kernel = np.array(sorted(nys_sizes)) * nb_classes
                                if wafter is True:
                                    nb_param_deepstrom = np.square(np.array(sorted(nys_sizes)))
                                else:
                                    nb_param_deepstrom = np.square(np.array(sorted(nys_sizes))) * nb_filter

                            np_param_nbr_deepstrom_kernel_k = (
                                    nb_param_deepstrom +  # m x m -> W matrix
                                    np.array(sorted(nys_sizes)) * output_conv_dim +  # m x d -> subsample storage
                                    nb_param_after_phi_deepstrom_kernel
                            )

                            ax.errorbar(np_param_nbr_deepstrom_kernel_k,
                                        np_deepstrom_kernel_k_mean_accuracy_test,
                                        np_deepstrom_kernel_k_std_accuracy_test,
                                        marker=real_nys_marker, color=linearity_color,
                                        label="Deepström",
                                         capsize=3)
                        #
                        # plot dense
                        # ==========
                        df_dense = df_cutlayer[df_cutlayer["method_name"] == "dense"]

                        df_dense["repr_dim"] = df_dense["repr_dim"].astype(np.int)
                        df_dense = df_dense.sort_values(by=["repr_dim"])

                        repr_dim = sorted(set(df_dense["repr_dim"].values))
                        logger.debug("Tested representation dimension are: {}".format(repr_dim))

                        np_dense_mean_accuracy_test = []
                        np_dense_std_accuracy_test = []
                        for d_size in repr_dim:
                            np_dense_mean_accuracy_test.append(np.mean(
                                df_dense[df_dense["repr_dim"] == d_size][
                                    "accuracy_test"]))
                            np_dense_std_accuracy_test.append(np.std(
                                df_dense[df_dense["repr_dim"] == d_size][
                                    "accuracy_test"]))

                        nb_param_after_phi_dense = np.array(sorted([int(n) for n in repr_dim])) * nb_classes

                        ax.errorbar(
                            np.array(sorted([int(n) for n in repr_dim])) * output_conv_dim +  # d x D
                            nb_param_after_phi_dense,
                            np_dense_mean_accuracy_test,
                            np_dense_std_accuracy_test,
                            color=dense_color,
                            marker=dense_marker,
                            label="Fully Connected", capsize=3)

                        # plot deepfried
                        # ==============
                        df_deepfried = df_cutlayer[df_cutlayer["method_name"] == "deepfriedconvnet"]
                        df_deepfried["nb_layer_deepfried"] = df_deepfried["nb_layer_deepfried"].astype(np.int)
                        df_deepfried = df_deepfried.sort_values(by=["nb_layer_deepfried"])

                        nb_layers_deepfried = sorted(set(df_deepfried["nb_layer_deepfried"].values))
                        logger.debug("nb layers deepfried: {}".format(nb_layers_deepfried))

                        np_deepfried_mean_accuracy_test = []
                        np_deepfried_std_accuracy_test = []
                        for l_nb in sorted(nb_layers_deepfried):
                            df_deepfried_stack = df_deepfried[df_deepfried["nb_layer_deepfried"] == l_nb]
                            np_deepfried_mean_accuracy_test.append(np.mean(df_deepfried_stack["accuracy_test"]))
                            np_deepfried_std_accuracy_test.append(np.std(df_deepfried_stack["accuracy_test"]))


                        nb_param_after_phi_deepfried = np.array([output_conv_dim * i * nb_classes for i in nb_layers_deepfried])

                        ax.errorbar(
                            np.array([(output_conv_dim * 3) * i for i in nb_layers_deepfried]) +
                            nb_param_after_phi_deepfried,
                            np_deepfried_mean_accuracy_test,
                            np_deepfried_std_accuracy_test,
                            color=deepfried_color,
                            marker=deepfried_marker,

                            label="DeepFriedConvnet", capsize=3)

                        ax.set_ylim(min_acc, max_acc)
                        ax.set_ylabel("Accuracy")
                        ax.set_xticks([1e4, 1e5, 1e6])
                        if i == 2:
                            ax.set_xlabel("# Parameters")
                        ax.set_xlabel("# Parameters")
                        ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=2)
                        ax.set_xticklabels([1e4, 1e5, 1e6])
                        # else:
                        #     ax.set_xticklabels([])
                        ax.set_xscale("symlog")

                        ax_twin = ax.twiny()
                        ax_twin.set_xscale("symlog")
                        ax_twin.set_xlim(ax.get_xlim())
                        ax_twin.set_xticks(np_param_nbr_deepstrom_kernel_w)

                        # if i == 0:
                        ax_twin.set_xlabel("Subsample Size")
                        title = "{} - {} - {} - wafter: {} - mulmet: {}".format(d_translate_kernel[k_name],
                                                                                         DATANAME, CUT_LAYER, wafter, multiview_method),
                        ax.set_title(title, y=1.2)
                        ax_twin.set_xticklabels(sorted(set(df_deepstrom_kernel_w["subsample_size"])))
                        # else:
                        #     ax.set_title("Noyau {} - {} - Train size: {}".format(d_translate_kernel[k_name], DATANAME, t_size))
                        #     ax_twin.set_xticklabels([])

                        f.set_size_inches(8, 6)
                        f.tight_layout()
                        f.subplots_adjust(bottom=0.3)
                        base_out_dir = os.path.join(os.path.abspath(__file__.split(".")[0]), "images")
                        out_dir_dataset = os.path.join(base_out_dir, DATANAME)
                        out_dir_cut_layer = os.path.join(out_dir_dataset, CUT_LAYER)

                        out_name = "acc_param_{}_{}_{}".format(k_name, wafter, multiview_method)

                        pathlib.Path(out_dir_cut_layer).mkdir(parents=True, exist_ok=True)
                        out_path = os.path.join(out_dir_cut_layer, out_name)
                        logger.debug(out_path)
                        f.savefig(out_path)
