
# coding: utf-8

# In[38]:


import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pathlib
import os

from skluc.main.utils import logger

matplotlib.rcParams.update({'font.size': 14})
pd.set_option('display.expand_frame_repr', False)


# In[2]:


def build_df():
    filepath = os.path.join(DIRNAME_BIG, FILENAME_BIG)
    df = pd.read_csv(filepath)
    df = df.apply(pd.to_numeric, errors="ignore")
    df = df.drop_duplicates()
    col_to_delete = ['--batch-size', '--chi-square-PD-kernel', '--chi-square-kernel',
       '--cifar10', '--cifar100', '--exp-chi-square-kernel',
       '--intercept-constant', '--laplacian-kernel', '--linear-kernel',
       '--mnist', '--quiet', '--rbf-kernel',
       '--sigmoid-kernel', '--stacked-kernel', '--sumed-kernel', '--svhn',
       '--tensorboard', '--validation-size',
       'deepfriedconvnet', 'deepstrom','dense', "--gamma", "--nb-stack", 
                     "--non-linear", "--non-linearity", "--num-epoch", "--seed",
                     "--train-size", "--second-layer-size", "activation_function",
                     "deepstrom_activation", "--real-fastfood", "--real-nystrom"
            ]
    for c in col_to_delete:
        df = df.drop([c], axis=1)

    return df

DIRNAME_BIG = "/home/luc/Resultats/Deepstrom/october_2018/classif_end_to_end"
FILENAME_BIG = "gathered_results.csv"
df = build_df()


# In[3]:


def get_sorted_acc_for_dataset(df_, dataset):
    df_dataset = df_[df_.dataset == dataset]
    df_dataset = df_dataset.sort_values(by="test_acc", ascending=False)
    return df_dataset


# In[4]:


get_sorted_acc_for_dataset(df, "mnist")


# In[5]:


get_sorted_acc_for_dataset(df, "cifar10")


# In[6]:


get_sorted_acc_for_dataset(df, "cifar100")


# In[7]:


get_sorted_acc_for_dataset(df, "svhn")


# In[10]:


method_names = set(df["network"].values)
kernel_names = set(df["kernel"].values)
kernel_names.remove("None")
repr_dim = set(df["--out-dim"].values)
repr_dim.remove("None")  # dtype: str
nys_size = set(df["--nys-size"].values)
nys_size.remove("None")
datasets = set(df["dataset"])

logger.debug("Nystrom possible sizes are: {}".format(nys_size))
logger.debug("Kernel functions are: {}".format(kernel_names))
logger.debug("Compared network types are: {}".format(method_names))
logger.debug("Tested representation dimension are: {}".format(repr_dim))


# In[43]:


nb_classes_datasets = {
    "svhn": 10,
    "cifar10": 10,
    "mnist": 10,
    "cifar100": 100
}

nb_feature_convs = {
    "svhn": 512,
    "cifar10": 512,
    "mnist": 16,
    "cifar100": 512
}

min_acc = 0
max_acc = 1

deepstrom_markers = {
    "linear": "x",
    "chi2_cpd": "o"
}

# In[46]:


def post_processing_figures(f, ax, nbparamdeepstrom, subsample_sizes):
    ax.set_ylim(min_acc, max_acc)
    ax.set_ylabel("Accuracy")
    ax.set_xticks([1e4, 1e5, 1e6])
    ax.set_xlabel("# Learnable Parameters")
    ax.legend(bbox_to_anchor=(0.5, -0.20), loc="upper center", ncol=2)
    ax.set_xticklabels([1e4, 1e5, 1e6])
    ax.set_xscale("symlog")

    ax_twin = ax.twiny()
    ax_twin.set_xscale("symlog")
    ax_twin.set_xlim(ax.get_xlim())
    ax_twin.set_xticks(sorted(nbparamdeepstrom))
    ax_twin.set_xticklabels(sorted(subsample_sizes))
    ax_twin.set_xlabel("Subsample Size")
    ax.set_title("{}".format(DATANAME), y=1.2)

    f.set_size_inches(8, 6)
    f.tight_layout()
    f.subplots_adjust(bottom=0.3)

    out_name = "end_to_end_{}".format(DATANAME)

    base_out_dir = os.path.abspath(__file__.split(".")[0])
    base_out_dir_path = pathlib.Path(base_out_dir) / "images"
    base_out_dir_path.mkdir(parents=True, exist_ok=True)
    out_path = base_out_dir_path / out_name
    logger.debug(out_path)
    f.savefig(out_path)


# In[47]:


for DATANAME in datasets:
    df_data = df[df["dataset"] == DATANAME]
    nb_classes_dataset = nb_classes_datasets[DATANAME]
    nb_feature_conv = nb_feature_convs[DATANAME]
    f, ax = plt.subplots()
    for k_name in kernel_names:
        df_kernel = df_data[df_data["kernel"] == k_name]
        accuracies_kernel = df_kernel["test_acc"]
        subsample_sizes_kernel = df_kernel["--nys-size"].astype(int)
        np_param = (np.square(subsample_sizes_kernel) +  # m x m
                    subsample_sizes_kernel * nb_classes_dataset)  # m x c
        sorted_idx = np.argsort(np_param.values)
        ax.plot(np_param.values[sorted_idx], accuracies_kernel.values[sorted_idx], color="g", marker=deepstrom_markers[k_name], label=f"Deepstrom {k_name}")

    
    df_dense = df_data[df_data["network"] == "dense"]
    accuracies_dense = df_dense["test_acc"]
    out_dim_dense = df_dense["--out-dim"].astype(int)
    np_param_dense = (nb_feature_conv * out_dim_dense +  # d x D
                out_dim_dense * nb_classes_dataset)  # D x c
    sorted_idx_dense = np.argsort(np_param_dense.values)
    ax.plot(np_param_dense.values[sorted_idx_dense], accuracies_dense.values[sorted_idx_dense], color="r", marker="s", label=f"Dense")
    post_processing_figures(f, ax, np_param, subsample_sizes_kernel)

