"""
Benchmark VGG: Benchmarking deepstrom versus other architectures of the VGG network.

Usage:
    benchmark_vgg [--mv|--cifar|--mnist] dense [-a value] [-v size] [-e numepoch] [-s batchsize] [-D reprdim] [-l]
    benchmark_vgg deepfriedconvnet [--mv|--cifar|--mnist] [-a value] [-v size] [-e numepoch] [-s batchsize] [-g gammavalue] [-N nbstack]
    benchmark_vgg deepstrom [--cifar|--mv|--mnist] [-r] [-a value] [-v size] [-e numepoch] [-s batchsize] [-D reprdim] [-m size] (-R|-L|-C|-E|-P|-S|-A|-T|-M) [-g gammavalue] [-c cvalue] [-n]

Options:
    --help -h                               Display help and exit.
    --cifar                                 Use cifar dataset
    --mnist                                 Use mnist dataset
    --mv                                    Use movie review dataset
    -e numepoch --num-epoch=numepoch        The number of epoch.
    -s batchsize --batch-size=batchsize     The number of example in each batch
    -D reprdim --out-dim=reprdim            The dimension of the final representation
    -l --two-layers                         Says if the dense network should have 1 or 2 layers (default 1).
    -m size --nys-size size                 The number of example in the nystrom subsample.
    -R --rbf-kernel                         Says if the rbf kernel should be used for nystrom.
    -L --linear-kernel                      Says if the linear kernel should be used for nystrom.
    -C --chi-square-kernel                  Says if the basic additive chi square kernel should be used for nystrom.
    -E --exp-chi-square-kernel              Says if the exponential chi square kernel should be used for nystrom.
    -P --chi-square-PD-kernel               Says if the Positive definite version of the basic additive chi square kernel should be used for nystrom.
    -S --sigmoid-kernel                     Says it the sigmoid kernel should be used for nystrom.
    -A --laplacian-kernel                   Says if the laplacian kernel should be used for nystrom.
    -T --stacked-kernel                     Says if the kernels laplacian, chi2 and rbf in a stacked setting should be used for nystrom.
    -M --sumed-kernel                       Says if the kernels laplacian, chi2 and rbf in a summed setting should be used for nystrom.
    -c cvalue --intercept-constant cvalue   The value of the intercept constant for the hyperbolic tangent kernel.
    -g gammavalue --gamma gammavalue        The value of gamma for rbf, chi or hyperbolic tangent kernel (deepstrom and deepfriedconvnet)
    -N nbstack --nb-stack nbstack           The number of fastfood stack for deepfriedconvnet
    -v size --validation-size size          The size of the validation set [default: 10000]
    -a value --seed value                   The seed value used for all randomization processed [default: 0]
    -r --real-nystrom                       Says if the matrix for deepstrom should be K^(-1/2)
    -n --non-linear                         Tell Nystrom to use the non linear activation function on its output.
"""
import sys
import time as t
import numpy as np
import tensorflow as tf
import docopt
from sklearn.metrics.pairwise import rbf_kernel, linear_kernel, additive_chi2_kernel, chi2_kernel, laplacian_kernel
import skluc.main.data.mldatasets as dataset
from skluc.main.tensorflow_.kernel_approximation.fastfood_layer import fastfood_layer
from skluc.main.tensorflow_.kernel_approximation.nystrom_layer import nystrom_layer
from skluc.main.tensorflow_.utils import fully_connected, batch_generator, classification_cifar
from skluc.main.tensorflow_.kernel import tf_rbf_kernel, tf_linear_kernel, tf_chi_square_CPD, tf_chi_square_CPD_exp, \
    tf_chi_square_PD, tf_sigmoid_kernel, tf_laplacian_kernel, tf_sum_of_kernels
from skluc.main.utils import logger


def fct_dense(input_, out_dim, two_layers):
    with tf.variable_scope("dense_layers"):
        fc_1 = fully_connected(input_, out_dim, act=tf.nn.relu, variable_scope="fc1")
        if two_layers:
            fc_2 = fully_connected(fc_1, out_dim, act=tf.nn.relu, variable_scope="fc2")
        else:
            fc_2 = fc_1
    out = fc_2
    return out


def fct_deepstrom(input_, out_dim, subsample, kernel, kernel_params, w_matrix, non_linearity):
    """
    Wrap the computing of the deepstrom layer

    :param input_:
    :param out_dim:
    :param subsample:
    :param kernel:
    :param kernel_params:
    :return:
    """
    out_fc = nystrom_layer(input_, subsample, W_matrix=w_matrix, output_dim=out_dim, kernel=kernel, output_act=non_linearity, **kernel_params)
    return out_fc


def fct_deepfried(input_, nb_stack, sigma):
    out_fc = fastfood_layer(input_, sigma, nbr_stack=nb_stack, trainable=True)
    return out_fc


if __name__ == '__main__':
    raise NotImplementedError("Not updated code.")
    # todo special treat for each type of execution
    arguments = docopt.docopt(__doc__)
    NUM_EPOCH = int(arguments["--num-epoch"])
    BATCH_SIZE = int(arguments["--batch-size"])
    OUT_DIM = int(arguments["--out-dim"]) if arguments["--out-dim"] is not None else None
    TWO_LAYERS_DENSE = arguments["--two-layers"]
    RBF_KERNEL = arguments["--rbf-kernel"]
    LINEAR_KERNEL = arguments["--linear-kernel"]
    CHI2_KERNEL = arguments["--chi-square-kernel"]
    CHI2_EXP_KERNEL = arguments["--exp-chi-square-kernel"]
    CHI2_PD_KERNEL = arguments["--chi-square-PD-kernel"]
    SIGMOID_KERNEL = arguments["--sigmoid-kernel"]
    LAPLACIAN_KERNEL = arguments["--laplacian-kernel"]
    STACKED_KERNEL = arguments["--stacked-kernel"]
    SUMED_KERNEL = arguments["--sumed-kernel"]
    VALIDATION_SIZE = int(arguments["--validation-size"])
    REAL_NYSTROM = arguments["--real-nystrom"]
    SEED = int(arguments["--seed"])
    NYS_SUBSAMPLE_SIZE = None
    KERNEL_NAME = None
    GAMMA = None
    CONST = None
    NB_STACK = None
    NON_LINEAR = tf.nn.relu if arguments["--non-linear"] else None
    kernel_dict = {}
    CIFAR_DATASET = bool(arguments["--cifar"])
    MNIST_DATASET = bool(arguments["--mnist"])
    MV_DATASET = bool(arguments["--mv"])

    SEED_TRAIN_VALIDATION = 0
    # noinspection PyUnboundLocalVariable
    TO_IMAGE = False
    if CIFAR_DATASET:
        data = dataset.Cifar10Dataset(validation_size=VALIDATION_SIZE, seed=SEED_TRAIN_VALIDATION)
        transformer = VGG19Cifar10Transformer
        TO_IMAGE = True
    elif MNIST_DATASET:
        data = dataset.MnistDataset(validation_size=VALIDATION_SIZE, seed=SEED_TRAIN_VALIDATION)
        transformer = LecunMnistTransformer
        TO_IMAGE = True
    elif MV_DATASET:
        data = dataset.MovieReviewV1Dataset(validation_size=VALIDATION_SIZE, seed=SEED_TRAIN_VALIDATION)
        transformer = tCNNMovieReviewTransformer
    else:
        raise ValueError("No dataset specified")
    data.load()
    if TO_IMAGE:
        data.normalize()
    data.data_astype(np.float32)
    data.labels_astype(np.float32)
    if TO_IMAGE:
        data.to_image()
    data.apply_transformer(transformer)
    if TO_IMAGE:
        data.to_one_hot()
        data.flatten()
    # exit()
    logger.debug("Start benchmark with parameters: {}".format(" ".join(sys.argv[1:])))
    logger.debug("Using dataset {} with validation size {} and seed for spliting set {}.".format(data.s_name, data.validation_size, data.seed))
    logger.debug("Shape of train set data: {}; shape of train set labels: {}".format(data.train[0].shape, data.train[1].shape))
    logger.debug("Shape of validation set data: {}; shape of validation set labels: {}".format(data.validation[0].shape, data.validation[1].shape))
    logger.debug("Shape of test set data: {}; shape of test set labels: {}".format(data.test[0].shape, data.test[1].shape))
    logger.debug("Sample of label: {}".format(data.train[1][0]))
    # todo separated function for parameters parsing

    if arguments["dense"]:
        NETWORK = "dense"
    elif arguments["deepstrom"]:
        NETWORK = "deepstrom"
        NYS_SUBSAMPLE_SIZE = int(arguments["--nys-size"])
        if OUT_DIM is None:
            OUT_DIM = NYS_SUBSAMPLE_SIZE
        if RBF_KERNEL:
            KERNEL = tf_rbf_kernel
            KERNEL_NAME = "rbf"
            GAMMA = float(arguments["--gamma"])
            kernel_dict = {"gamma": GAMMA}
        elif LINEAR_KERNEL:
            KERNEL = tf_linear_kernel
            KERNEL_NAME = "linear"
        elif CHI2_KERNEL:
            KERNEL = tf_chi_square_CPD
            KERNEL_NAME = "chi2_cpd"
        elif CHI2_EXP_KERNEL:
            KERNEL = tf_chi_square_CPD_exp
            KERNEL_NAME = "chi2_exp_cpd"
            GAMMA = float(arguments["--gamma"])
            kernel_dict = {"gamma": GAMMA}
        elif CHI2_PD_KERNEL:
            KERNEL = tf_chi_square_PD
            KERNEL_NAME = "chi2_pd"
        elif SIGMOID_KERNEL:
            KERNEL = tf_sigmoid_kernel
            KERNEL_NAME = "sigmoid"
            GAMMA = float(arguments["--gamma"])
            CONST = float(arguments["--intercept-constant"])
            kernel_dict = {"gamma": GAMMA, "constant": CONST}
        elif LAPLACIAN_KERNEL:
            KERNEL = tf_laplacian_kernel
            KERNEL_NAME = "laplacian"
            GAMMA = float(arguments["--gamma"])
            kernel_dict = {"gamma": np.sqrt(GAMMA)}
        elif STACKED_KERNEL:
            GAMMA = float(arguments["--gamma"])
        #
        #     def KERNEL(X, Y):
        #         return tf_stack_of_kernels(X, Y,
        #                                    [tf_laplacian_kernel, tf_rbf_kernel, tf_chi_square_CPD],
        #                                    [{"gamma": GAMMA}, {"gamma": GAMMA}, {}])
            KERNEL_NAME = "stacked"
        #     # todo it doesn't work
        elif SUMED_KERNEL:
            GAMMA = float(arguments["--gamma"])

            def KERNEL(X, Y):
                return tf_sum_of_kernels(X, Y,
                                           [tf_laplacian_kernel, tf_rbf_kernel, tf_chi_square_CPD],
                                           [{"gamma": GAMMA}, {"gamma": GAMMA}, {}])
            KERNEL_NAME = "summed"
        else:
            raise Exception("No kernel function specified for deepstrom")

    elif arguments["deepfriedconvnet"]:
        NETWORK = "deepfriedconvnet"
        NB_STACK = int(arguments["--nb-stack"])
        GAMMA = float(arguments["--gamma"])
        SIGMA = 1 / GAMMA
    else:
        raise Exception("Not recognized network")

    input_dim, output_dim = data.train[0].shape[1], data.train[1].shape[1]

    x = tf.placeholder(tf.float32, shape=[None, input_dim], name="x")
    y = tf.placeholder(tf.float32, shape=[None, output_dim], name="label")

    if NETWORK == "dense":
        logger.debug("Selecting dense layer function with output dim = {} and {} layers".format(OUT_DIM, 2 if TWO_LAYERS_DENSE else 1))
        input_classif = fct_dense(x, OUT_DIM, TWO_LAYERS_DENSE)

    elif NETWORK == "deepstrom":
        logger.debug("Selecting {} deepstrom layer function with "
                     "subsample size = {}, "
                     "output_dim = {}, "
                     "{} activation function "
                     "and kernel = {}"
                     .format("real" if REAL_NYSTROM else "learned",
                             NYS_SUBSAMPLE_SIZE,
                             OUT_DIM,
                             "with" if NON_LINEAR else "without",
                             KERNEL_NAME))
        if OUT_DIM is not None and OUT_DIM > NYS_SUBSAMPLE_SIZE:
            logger.debug("Output dim is greater than deepstrom subsample size. Aborting.")
            # todo change this because it is copy-pasted (use function instead)

            global_acc_val = None
            global_acc_test = None
            training_time = None
            printed_r_list = [str(NETWORK),
                      str(global_acc_val),
                      str(global_acc_test),
                      str(training_time),
                      str(NUM_EPOCH),
                      str(BATCH_SIZE),
                      str(OUT_DIM),
                      str(TWO_LAYERS_DENSE),
                      str(KERNEL_NAME),
                      str(GAMMA),
                      str(CONST),
                      str(NB_STACK),
                      str(NYS_SUBSAMPLE_SIZE),
                      str(VALIDATION_SIZE),
                      str(SEED),
                      str(NON_LINEAR),
                      str(REAL_NYSTROM)
                      ]
            print(",".join(printed_r_list))
            exit()
        np.random.seed(SEED)
        nys_subsample_index = np.random.permutation(data.train.data.shape[0])
        nys_subsample = data.train.data[nys_subsample_index[:NYS_SUBSAMPLE_SIZE]]
        if REAL_NYSTROM:
            logger.debug("Real nystrom asked: eg projection matrix has the vanilla formula")
            if KERNEL_NAME == "rbf":
                kernel_fct = rbf_kernel
            elif KERNEL_NAME == "linear":
                kernel_fct = linear_kernel
            elif KERNEL_NAME == "chi2_cpd":
                kernel_fct = additive_chi2_kernel
            elif KERNEL_NAME == "chi2_exp_cpd":
                kernel_fct = chi2_kernel
            elif KERNEL_NAME == "chi2_pd":
                raise NotImplementedError("Bien verifier que ce code ne fait pas bordel")
                # todo wtf?
                # def tf_chi_square_PD(X, Y):
                #     # the drawing of the matrix X expanded looks like a wall
                #     wall = np.expand_dims(X, axis=1)
                #     # the drawing of the matrix Y expanded looks like a floor
                #     floor = np.expand_dims(Y, axis=0)
                #     numerator = 2 * wall * floor
                #     denominator = wall + floor
                #     quotient = numerator / denominator
                #     quotient_without_nan = np.where(np.isnan(quotient), np.zeros_like(quotient), quotient)
                #     K = np.sum(quotient_without_nan, axis=2)
                #     return K
                # kernel_fct = tf_chi_square_PD
            elif KERNEL_NAME == "laplacian":
                kernel_fct = laplacian_kernel
            else:
                raise ValueError("Unknown kernel name: {}".format(KERNEL_NAME))
            K11 = kernel_fct(nys_subsample, nys_subsample, **kernel_dict)
            U, S, V = np.linalg.svd(K11)
            invert_root_K11 = np.dot(U / np.sqrt(S), V).astype(np.float32)
            input_classif = fct_deepstrom(x, OUT_DIM, nys_subsample, KERNEL, kernel_dict, w_matrix=invert_root_K11, non_linearity=NON_LINEAR)
        else:
            if STACKED_KERNEL:
                possible_kernels = [
                    tf_rbf_kernel,
                    tf_linear_kernel,
                    tf_chi_square_CPD
                ]
                parameters_kernels = [
                    {"gamma": GAMMA},
                    {},
                    {}
                ]
                lst_input_classif = []
                for i, k_fct in enumerate(possible_kernels):
                    k_params = parameters_kernels[i]
                    with tf.variable_scope("kernel_{}".format(k_fct.__name__)):
                        lst_input_classif.append(fct_deepstrom(x, OUT_DIM, nys_subsample, k_fct, k_params, w_matrix=None, non_linearity=NON_LINEAR))
                input_classif = tf.concat(lst_input_classif, axis=1, name="Kstack")
            else:
                input_classif = fct_deepstrom(x, OUT_DIM, nys_subsample, KERNEL, kernel_dict, w_matrix=None, non_linearity=NON_LINEAR)

    elif NETWORK == "deepfriedconvnet":
        logger.debug("Selecting deepfriedconvnet layer function")
        input_classif = fct_deepfried(x, NB_STACK, SIGMA)

    else:
        raise Exception("Not recognized network")

    classif, keep_prob = classification_cifar(input_classif, output_dim)

    # calcul de la loss
    with tf.name_scope("xent"):
        cross_entropy = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=classif, name="xentropy"),
            name="xentropy_mean")
        tf.summary.scalar('loss-xent', cross_entropy)

    # todo learning rate as hyperparameter
    # calcul du gradient
    with tf.name_scope("train"):
        global_step = tf.Variable(0, name="global_step", trainable=False)
        train_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cross_entropy,
                                                                              global_step=global_step)

    # calcul de l'accuracy
    with tf.name_scope("accuracy"):
        predictions = tf.argmax(classif, 1)
        correct_prediction = tf.equal(predictions, tf.argmax(y, 1))
        accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.summary.scalar("accuracy", accuracy_op)

    merged_summary = tf.summary.merge_all()

    init = tf.global_variables_initializer()
    # Create a session for running Ops on the Graph.
    # Instantiate a SummaryWriter to output summaries and the Graph.
    # summary_writer = tf.summary.FileWriter("debug_benchmark_vgg")
    # Initialize all Variable objects
    # actual learning
    with tf.Session() as sess:
        # summary_writer.add_graph(sess.graph)
        # Initialize all Variable objects
        sess.run(init)
        # actual learning
        feed_dict_val = {x: data.validation.data, y: data.validation.labels, keep_prob: 1.0}
        global_start = t.time()
        for i in range(NUM_EPOCH):
            j = 0
            start = t.time()
            for X_batch, Y_batch in batch_generator(data.train.data, data.train.labels, BATCH_SIZE, True):
                feed_dict = {x: X_batch, y: Y_batch, keep_prob: 0.5}
                _, loss, acc = sess.run([train_optimizer, cross_entropy, accuracy_op], feed_dict=feed_dict)
                if j % 100 == 0:
                    logger.debug("epoch: {}/{}; batch: {}/{}; loss: {}; acc: {}".format(i, NUM_EPOCH, j, int(data.train[0].shape[0]/BATCH_SIZE), loss, acc))
                    acc = sess.run([accuracy_op], feed_dict=feed_dict_val)
                    logger.debug("Validation accuracy: {}".format(acc))

                    # summary_str = sess.run(merged_summary, feed_dict=feed_dict)
                    # summary_writer.add_summary(summary_str, j)
                j += 1

        training_time = t.time() - global_start
        accuracies_val = []
        i = 0
        for X_batch, Y_batch in batch_generator(data.validation.data, data.validation.labels, 1000, False):
            accuracy = sess.run([accuracy_op], feed_dict={
                x: X_batch, y: Y_batch, keep_prob: 1.0})
            accuracies_val.append(accuracy[0])
            i += 1

        accuracies_test = []
        j = 0
        for X_batch, Y_batch in batch_generator(data.test[0], data.test[1], 1000, False):
            accuracy = sess.run([accuracy_op], feed_dict={
                x: X_batch, y: Y_batch, keep_prob: 1.0})
            accuracies_test.append(accuracy[0])
            j += 1

    global_acc_val = sum(accuracies_val) / i
    global_acc_test = sum(accuracies_test) / j
    printed_r_list = [str(NETWORK),
                      str(global_acc_val),
                      str(global_acc_test),
                      str(training_time),
                      str(NUM_EPOCH),
                      str(BATCH_SIZE),
                      str(OUT_DIM),
                      str(TWO_LAYERS_DENSE),
                      str(KERNEL_NAME),
                      str(GAMMA),
                      str(CONST),
                      str(NB_STACK),
                      str(NYS_SUBSAMPLE_SIZE),
                      str(VALIDATION_SIZE),
                      str(SEED),
                      str(NON_LINEAR),
                      str(REAL_NYSTROM)
                      ]
    print(",".join(printed_r_list))
