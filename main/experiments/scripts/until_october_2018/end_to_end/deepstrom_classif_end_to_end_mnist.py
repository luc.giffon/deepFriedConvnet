
# coding: utf-8

# In[1]:


import skluc.main.data.mldatasets as dataset
import numpy as np
import tensorflow as tf
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from tensorflow.python.keras.models import Sequential
from skluc.main.tensorflow_.kernel_approximation.nystrom_layer import DeepstromLayerEndToEnd
from skluc.main.utils import logger, memory_usage
from skluc.main.tensorflow_.utils import batch_generator
import time as t

NUM_EPOCH = 200
BATCH_SIZE = 128
VALIDATION_SIZE = 10000
SEED_TRAIN_VALIDATION = 0
SUBSAMPLE_SIZE = 128
KERNEL_NAME = 'chi2_cpd'
kernel_dict = {}


# # Data loading

# In[2]:


data = dataset.MnistDataset(validation_size=VALIDATION_SIZE, seed=SEED_TRAIN_VALIDATION)
data.load()
data.to_one_hot()
data.to_image()
data.data_astype(np.float32)
data.labels_astype(np.float32)
data.normalize()

X_train, y_train = data.train.data, data.train.labels
X_test, y_test = data.test.data, data.test.labels
X_val, y_val = data.validation.data, data.validation.labels


# # Model definition

# In[4]:


def build_lenet_model(input_shape):
    model = Sequential()
    model.add(
        Conv2D(6, (5, 5), padding='valid', activation='relu', kernel_initializer='he_normal', input_shape=input_shape))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))
    model.add(Conv2D(16, (5, 5), padding='valid', activation='relu', kernel_initializer='he_normal'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2)))
    model.add(Flatten())
    return model


# In[5]:


input_dim = X_train.shape[1:]
output_dim = y_train.shape[1]

x = tf.placeholder(tf.float32, shape=[None, *input_dim], name="x")
y = tf.placeholder(tf.float32, shape=[None, output_dim], name="label")
subs = tf.placeholder(tf.float32, shape=[SUBSAMPLE_SIZE, *input_dim], name="subsample")

convnet_model = build_lenet_model(x.shape[1:])

repr_x = convnet_model(x)
repr_sub = convnet_model(subs)

deepstrom_layer = DeepstromLayerEndToEnd(subsample_size=SUBSAMPLE_SIZE,
    kernel_name=KERNEL_NAME)

deepstrom_output = deepstrom_layer([repr_x, repr_sub])

with tf.variable_scope("classification"):
    classif = Dense(output_dim)(deepstrom_output)

# calcul de la loss
with tf.name_scope("xent"):
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=classif, name="xentropy"),
        name="xentropy_mean")
    tf.summary.scalar('loss-xent', cross_entropy)

# todo learning rate as hyperparameter
# calcul du gradient
with tf.name_scope("train"):
    global_step = tf.Variable(0, name="global_step", trainable=False)
    train_optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cross_entropy,
                                                                          global_step=global_step)

# calcul de l'accuracy
with tf.name_scope("accuracy"):
    predictions = tf.argmax(classif, 1)
    correct_prediction = tf.equal(predictions, tf.argmax(y, 1))
    accuracy_op = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.summary.scalar("accuracy", accuracy_op)

merged_summary = tf.summary.merge_all()


# In[6]:


subsample_indexes = data.get_uniform_class_rand_indices_validation(SUBSAMPLE_SIZE)
nys_subsample = data.validation.data[subsample_indexes]
init = tf.global_variables_initializer()
summary_writer = tf.summary.FileWriter("debug_classification_end_to_end")


# In[7]:


with tf.Session() as sess:
       logger.info("Start training")
       summary_writer.add_graph(sess.graph)
       # Initialize all Variable objects
       sess.run(init)
       # actual learning
       # feed_dict_val = {x: data.validation[0], y: data.validation[1], keep_prob: 1.0}
       global_start = t.time()
       j = 0
       for i in range(NUM_EPOCH):
           logger.debug(memory_usage())

           start = t.time()
           for X_batch, Y_batch in batch_generator(X_train, y_train, BATCH_SIZE, False):
               feed_dict = {x: X_batch, y: Y_batch, subs: nys_subsample}
               _, loss, acc, summary_str = sess.run([train_optimizer, cross_entropy, accuracy_op, merged_summary], feed_dict=feed_dict)
               if j % 100 == 0:
                   logger.info(
                       "epoch: {}/{}; batch: {}/{}; batch_shape: {}; loss: {}; acc: {}".format(i, NUM_EPOCH, j + 1,
                                                                                               int(data.train[0].shape[
                                                                                                       0] / BATCH_SIZE) + 1,
                                                                                               X_batch.shape, loss,
                                                                                               acc))
                   summary_writer.add_summary(summary_str, j)
               j += 1

       logger.info("Evaluation on validation data")
       training_time = t.time() - global_start
       accuracies_val = []
       i = 0
       val_eval_start = t.time()
       for X_batch, Y_batch in batch_generator(data.validation.data, data.validation.labels, 1000, False):
           accuracy = sess.run([accuracy_op], feed_dict={
               x: X_batch, y: Y_batch, subs: nys_subsample})
           accuracies_val.append(accuracy[0])
           i += 1
       global_acc_val = sum(accuracies_val) / i
       VAL_EVAL_TIME = t.time() - val_eval_start
       logger.debug(f"Val acc = {global_acc_val}")

       logger.info("Evaluation on test data")
       accuracies_test = []
       i = 0
       test_eval_start = t.time()
       for X_batch, Y_batch in batch_generator(data.test.data, data.test.labels, 1000, False):
           accuracy = sess.run([accuracy_op], feed_dict={
               x: X_batch, y: Y_batch, subs: nys_subsample})
           accuracies_test.append(accuracy[0])
           i += 1
       global_acc_test = sum(accuracies_test) / i
       TEST_EVAL_TIME = t.time() - test_eval_start
       logger.debug(f"test acc = {global_acc_test}")

