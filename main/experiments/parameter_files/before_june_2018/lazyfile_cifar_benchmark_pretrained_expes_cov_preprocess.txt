dense -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -D 1024
dense -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -D 16
dense -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -D 64
dense -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -D 128
dense -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -D 1024
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 16 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 32 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 64 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 128 -r -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 16 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 16 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 32 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 32 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 64 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 64 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 128 -r -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 128 -r -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 16 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 32 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 64 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 128 -g 2.5959038894034687e-12 -R
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 0 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 1 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 2 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 3 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 4 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 5 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 6 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 7 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 8 -B 5 --cifar -m 128 -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 16 -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 16 -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 32 -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 32 -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 64 -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 64 -L
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 128 -C
deepstrom -e 50 -s 64 -v 10000 -a 9 -B 5 --cifar -m 128 -L
